
/***************************************************************************
 *   Copyright (C) 2008 by Denis V. Meltsaykin                             *
 *   dvm.avgoor@gmail.com                                                  *
 *                                                                         *
 *   Permission is hereby granted, free of charge, to any person obtaining *
 *   a copy of this software and associated documentation files (the       *
 *   "Software"), to deal in the Software without restriction, including   *
 *   without limitation the rights to use, copy, modify, merge, publish,   *
 *   distribute, sublicense, and/or sell copies of the Software, and to    *
 *   permit persons to whom the Software is furnished to do so, subject to *
 *   the following conditions:                                             *
 *                                                                         *
 *   The above copyright notice and this permission notice shall be        *
 *   included in all copies or substantial portions of the Software.       *
 *                                                                         *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       *
 *   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    *
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*
 *   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR     *
 *   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, *
 *   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR *
 *   OTHER DEALINGS IN THE SOFTWARE.                                       *
 ***************************************************************************/
#define BUFF 512

#define CRLF '\n'

#include "nms.h"
void
s_handler_bulk (int signum) {
	write_log (1, "ARRIVED %d, continuing", signum);
};


int
strip_space (char *to_strip, char *out) {

    char *ptr = to_strip;
    char buf[512];
    char *bufstr = buf;
    char prev;
    int count;

    count = 0;
    memset (&buf, '\0', 512);
    while (*ptr != '\0') {
        if (*ptr == ' ')
            if (prev == ' ') {
                prev = *ptr;
                (void) *ptr++;
            } else {
                *bufstr = *ptr;
                prev = *ptr;
                (void) *ptr++;
                (void) *bufstr++;
                count++;
            }
        else {
            *bufstr = *ptr;
            prev = *ptr;
            (void) *bufstr++;
            count++;
            (void) *ptr++;
        }
    };
    memcpy (out, buf, count);
    return count;
}

/*
 * char* catstring(char *dest, const char *src, size_t n){ size_t dest_len =
 * strlen(dest); size_t i;
 *
 * for (i = 0 ; i < n; i++) { if (src[i]!='\0') dest[dest_len + i] = src[i]; else
 * dest[dest_len + i] = '!'; }; dest[dest_len + i] = '\0';
 *
 * return dest; }
 */
char *
catstring (char *dest, char *src, size_t n) {
    char *r = dest;
    int i = 0;
    while (*dest != '\0')
        dest++;
    while (i < n) {
        if (*src != '\0') {
            *dest++ = *src++;
            i++;
        } else {
            // *dest++ = '~';
            *src++;
            i++;
        };
    };
    *dest = '\0';

    return r;
};

rdb
configuration_parse (char *cfg, pdb * profs, int sw_type) {
    pcre *f;
    // char *cfg
    pcre_extra *f_ext;
    char *pattern =
        ".*profile_id\\s(\\d+).*access_id\\s+(\\d+)\\s+(.*)\\sport\\s(\\d+)\\s(.*)\\r\\n";
    char *pattern_3028 =
        ".*profile_id\\s(\\d+)\\s+add\\saccess_id\\s+(\\d+)\\s+(.*\\S)\\s+port\\s(\\d+)\\s(.*)\\n\\r";
    // config access_profile profile_id 40 add access_id 1 ip source_ip 0.0.0.0 port 5
    // permit
    char *prof_pat = ".*(create access_profile .*\\d\\s)\\r";
    char *prof_pat_3028 = "(create.*access_profile.*\\n)\\r";
    const char *errstr;
    int errchar;
    int vector[10000];
    int vecsize = 10000;
    int pairs = 0;
    const char *vid;
    const char *acid;
    const char *port;
    const char *action;
    const char *subj;
    const char *prof_temp;
    char prof[512];
    char rule[512];
    char *profile_pattern;
    char *rule_pattern;

    const unsigned char *tables;
    int j;
    rdb cfg_list = NULL;
    pdb prof_list = NULL;

    // setlocale (LC_ALL, "en_US.UTF-8");
    tables = pcre_maketables ();

    if (sw_type == DES3028) {
        profile_pattern = prof_pat_3028;
        rule_pattern = pattern_3028;
    } else {
        profile_pattern = prof_pat;
        rule_pattern = pattern;
    };
    write_log (2, "TELNET_MOD:Start configuration parse");
    if ((f = pcre_compile (rule_pattern, PCRE_CASELESS | PCRE_MULTILINE, &errstr,
                           &errchar, tables)) == NULL) {
        write_log (2,
                   "TELNET_MOD: pcre pattern error %s\nSymb N%i\nPattern:%s\n",
                   errstr, errchar, pattern);
    } else {
        f_ext = pcre_study (f, 0, &errstr);
        j = 0;
        while ((pairs =
                    pcre_exec (f, f_ext, cfg, strlen (cfg), j, PCRE_NOTEMPTY,
                               vector, vecsize)) >= 0) {

            pcre_get_substring (cfg, vector, pairs, 1, &vid);
            pcre_get_substring (cfg, vector, pairs, 2, &acid);
            pcre_get_substring (cfg, vector, pairs, 3, &subj);
            pcre_get_substring (cfg, vector, pairs, 4, &port);
            pcre_get_substring (cfg, vector, pairs, 5, &action);

            memset (&rule, '\0', 512);
            if (sw_type == DES3028)
                strip_space ((char *) subj, rule);
            else
                memcpy (rule, (char *) subj, strlen (subj));

            if (strstr (action, "permit"))
                rdb_insert (&cfg_list, rule, atoi (port),
                            atoi (acid), atoi (vid), PERMIT);
            else
                rdb_insert (&cfg_list, (char *) rule, atoi (port),
                            atoi (acid), atoi (vid), DENY);

            pcre_free_substring (vid);
            pcre_free_substring (acid);
            pcre_free_substring (subj);
            pcre_free_substring (port);
            pcre_free_substring (action);
            j = vector[1] + 1;
        }
    }
    free (f_ext);
    pcre_free (f);
    if ((f = pcre_compile (profile_pattern, PCRE_CASELESS | PCRE_MULTILINE, &errstr,
                           &errchar, tables)) == NULL) {
        write_log (2,
                   "TELNET_MOD: pcre pattern error %s\nSymb N%i\nPattern:%s\n",
                   errstr, errchar, pattern);
    } else {
        f_ext = pcre_study (f, 0, &errstr);
        j = 0;
        while ((pairs = pcre_exec (f, f_ext, cfg, strlen (cfg), j, PCRE_NOTEMPTY,
                                   vector, vecsize)) >= 0) {

            pcre_get_substring (cfg, vector, pairs, 1, &prof_temp);
            memset (&prof, '\0', 512);
            if (sw_type == DES3028)
                strip_space ((char *) prof_temp, prof);
            else {
                memcpy (prof, (char *) prof_temp, strlen (prof_temp));
                prof[strlen (prof) - 1] = '\n';
            }
            pdb_insert (&prof_list, prof);
            pcre_free_substring (prof_temp);
            // free(prof);
            j = vector[1] + 1;
        }
    }

    write_log (2, "TELNET_MOD:End configuration parse");
    free (f_ext);
    pcre_free (f);
    free ((void *) tables);
    (*profs) = prof_list;
    return cfg_list;
};

int
acl_update (char *host_addr, char *login,
            char *pass, char *delimiter, int act_type,
            PGconn * resurs, char *copy, int sw_type) {

    int is_error = 0;		/* error state, 0 - no, 1 - yes */
    struct sockaddr_in address;
    struct in_addr inaddr;
    struct timeval timeo;
	struct timeval socktime;
    int sock;
    int out;
    int sopts;
    fd_set rd;
    int fail_count = 0;
    char errbuf[256];
    char query[250];
    extern opt *opts;

    int delimiter_size = strlen (delimiter);
    PGresult *result;

    write_log (2, "TELNET_MOD: [%s] acl_update() started", host_addr);
    // char *copy = my_malloc (opts->malloc_size); // memory not allocating each round

    char buffer[BUFF];
    size_t count;
    char filename[32];

    rdb sw_config = NULL;
    rdb db_config = NULL;
    pdb profiles1 = NULL;
    pdb sw_prof = NULL;
    pdb resulting = NULL;

    sprintf (filename, "%s/%s.acl.log", opts->sw_log_dir, host_addr);
    if ((out = open (filename, O_WRONLY | O_CREAT | O_TRUNC | O_NONBLOCK, (mode_t) 0666)) < 0)
        perror ("open");

    if ((sock = socket (PF_INET, SOCK_STREAM, 0)) < 0) {
        strerror_r (errno, errbuf, 256);	/* thread-safe, i guess */
        write_log (1, "(!) Socket failed: %s", errbuf);
        // free(copy);
        close (out);
        return (11);
    };

    socktime.tv_sec = 20;
    socktime.tv_usec = 0;
    setsockopt (sock, SOL_SOCKET, SO_SNDTIMEO, &socktime, sizeof (socktime));
    fcntl(sock, F_SETOWN, getpid());
    socktime.tv_sec = 20;
    socktime.tv_usec = 0;
    setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, &socktime, sizeof (socktime));

	signal (SIGPIPE, s_handler_bulk);
	signal (SIGIO, s_handler_bulk);
	signal (SIGURG, s_handler_bulk);


    address.sin_family = AF_INET;
    address.sin_port = htons (23);

    inet_aton (host_addr, &inaddr);
    memcpy (&address.sin_addr, &inaddr.s_addr, sizeof (address.sin_addr));
    if (connect (sock, (struct sockaddr *) &address, sizeof (address))) {
        strerror_r (errno, errbuf, 256);
        write_log (1, "TELNET_MOD: acl_update: (!) Connection to %s failed: %s",
                   host_addr, errbuf);
        // free (copy);
        close (out);
        close (sock);
        return errno;
    };

    memset (buffer, 0, BUFF);

    FD_ZERO (&rd);
    FD_SET (sock, &rd);

//    sopts = fcntl (sock, F_GETFL);
//    sopts = (sopts | O_NONBLOCK);
//    fcntl (sock, F_SETFL, sopts);


    do {

        FD_ZERO (&rd);
        FD_SET (sock, &rd);
	    
	    timeo.tv_sec = 10;
	    timeo.tv_usec = 0;

        if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0) {
            count = recv (sock, buffer, BUFF, 0);
            write (out, buffer, count);
            if (strstr (buffer, "Fail!")) {
                write_log (1, "TELNET_MOD: (!) [%s] Login to switch failed", host_addr);
                close (sock);
                // free (copy);
                close (out);
                return 11;
            };
            if (strstr (buffer, "ame:")) {

                FD_ZERO (&rd);
                FD_SET (sock, &rd);
                timeo.tv_sec = 5;
                timeo.tv_usec = 0;

                if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0)
                    send (sock, login, strlen (login), 0);
                else {
                    write_log (1, "TELNET_MOD: [%s] acl_update() Timeout in send login",
                               host_addr);
                    close (out);
                    close (sock);
                    return -1;
                };
                memset (buffer, 0, BUFF);
                write_log (3, "TELNET_MOD: (!) [%s] User name send", host_addr);
            };

            if (strstr (buffer, "ord:")) {
                memset (buffer, 0, BUFF);
                FD_ZERO (&rd);
                FD_SET (sock, &rd);
                timeo.tv_sec = 5;
                timeo.tv_usec = 0;

                if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0)
                    send (sock, pass, strlen (pass), 0);
                else {
                    write_log (1,
                               "TELNET_MOD: [%s] acl_update() Timeout in send password",
                               host_addr);
                    close (out);
                    close (sock);
                    return -1;
                };

                write_log (3, "TELNET_MOD: (!) [%s] Password send", host_addr);
            };
        } else {
            write_log (1, "TELNET_MOD: [%s] acl_update() Timeout in login", host_addr);
            close (out);
            close (sock);
            // free (copy);

            return -1;
        };
    } while (!strstr (buffer, delimiter));

    write (out, buffer, count);
    memset (buffer, 0, BUFF);

    timeo.tv_sec = 5;
    timeo.tv_usec = 0;
    FD_ZERO (&rd);
    FD_SET (sock, &rd);

    if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0)
        send (sock, "enable clipaging\n", 17, 0);
    else
        write_log (1, "TELNET_MOD: [%s] acl_update() Timeout in send(en clip)",
                   host_addr);
    count=0;
	memset (buffer, 0, BUFF);
	
	do {
	    timeo.tv_sec = 5;
		timeo.tv_usec = 0;
    	FD_ZERO (&rd);
	    FD_SET (sock, &rd);
	    if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0){
			count = recv (sock, buffer, BUFF, 0);
	    	write (out, buffer, count);
	    }
	    else { 
			write_log (1, "TELNET_MOD: [%s] acl_update() Timeout in action", host_addr); break; 
		}
	} while (!strstr (buffer, delimiter));

	
		memset (buffer, 0, BUFF);

        timeo.tv_sec = 10;
        timeo.tv_usec = 0;
        FD_ZERO (&rd);
        FD_SET (sock, &rd);

        if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0){
              send (sock, "show config current_config\n", 28, 0);
		}
        else {
            /*
             * Если мы не смогли послать комманду на
             * показ конфигурации, какой смысл
             * продолжать? :)
             */
            write_log (1, "TELNET_MOD: [%s] acl_update() Timeout in send(sh con)",
                       host_addr);
            goto abandon;
        };

        do {
            timeo.tv_sec = 10;
            timeo.tv_usec = 0;
            FD_ZERO (&rd);
            FD_SET (sock, &rd);
            if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0) {
                count = recv (sock, buffer, BUFF, 0);
                catstring (copy, buffer, count);
                write (out, buffer, count);

                if (memmem (buffer, count, delimiter, delimiter_size)) {
               		write (out, "\nTHROW OUT\n", 11);	
                    goto here;
                 };
                if (strstr (buffer, "[0m")) {	// clipped page
                    fail_count = 0;
                    while (fail_count < 2) {
                        timeo.tv_sec = 2;
                        timeo.tv_usec = 0;
                        FD_ZERO (&rd);
                        FD_SET (sock, &rd);
                        if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0) {
                            send (sock, "n", 1, 0);
                            break;
                        } else {
                            write_log (1,
                                       "TELNET_MOD: [%s] acl_update() Timeout in send(n)",
                                       host_addr);
                            fail_count++;
                        };
                    };

                    if (memmem (buffer, count, delimiter, delimiter_size))
                        goto here;

                    memset (buffer, 0, BUFF);

                };

            } else {
                write_log (1, "TELNET_MOD: (!)[%s] acl_update: config read timeout",
                           host_addr);
                goto abandon;
            }
        } while (!memmem (buffer, count, delimiter, delimiter_size));


here:

    memset (buffer, '\0', sizeof buffer);
    write (out, "\n--COPY-\n", 9);
    write (out, copy, strlen (copy));
    write (out, "\n--DATA-\n", 9);
    write_log (2, "TELNET_MOD: [%s] Got configuration (%d bytes)", host_addr,
               strlen (copy));
    /*
     * HERE WE PARSING CFG
     */
    db_config = make_rules_db (host_addr, &profiles1, resurs, act_type, sw_type);	// перенес
    // сюда
    // для
    // порядку
    // и
    // удобства

    sw_config = configuration_parse (copy, &sw_prof, sw_type);

    rdb tmp;
    pdb tmp_p;

    if (sw_config) {
        tmp = sw_config->head;
        while (tmp) {
            dprintf (out, "rule '%s'\t port '%d'\t acid '%d'\t prof '%d'\t act '%d'\n",
                     tmp->string, tmp->port, tmp->access_id, tmp->profile_id,
                     tmp->action);
            tmp = tmp->next;
        };
    };
    if (sw_prof) {
        tmp_p = sw_prof->head;
        while (tmp_p) {
            dprintf (out, "profile '%s'\n", tmp_p->profile);
            tmp_p = tmp_p->next;
        };
    };
    write (out, "\nPREDATA\n", 9);
    if (db_config) {
        tmp = db_config->head;
        while (tmp) {
            dprintf (out, "rule '%s'\t port '%d'\t acid '%d'\t prof '%d'\t act '%d'\n",
                     tmp->string, tmp->port, tmp->access_id, tmp->profile_id,
                     tmp->action);
            tmp = tmp->next;
        };
    };

    if (profiles1) {
        tmp_p = profiles1->head;
        while (tmp_p) {
            dprintf (out, "profile '%s'\n", tmp_p->profile);
            tmp_p = tmp_p->next;
        };
    };

    char delimt[18] = "\n----RESULT!----\n";
    /*
     * if (sw_config) { tmp=sw_config->head;
     *
     * write (out, delimt, 18); while(tmp) { //printf("%s\n",
     * tmp->string); write (out, tmp->string, strlen(tmp->string));
     * tmp=tmp->next; } }
     */
    write_log (2, "TELNET_MOD: [%s] acl_update: in progress", host_addr);

    resulting = pdb_compare (&sw_prof, &profiles1);

    rdb_compare (&sw_config, &db_config, &resulting, out);

    pdb temp2;

    if (resulting) {
        write_log (1, "TELNET_MOD: [%s] switch need updates", host_addr);
        temp2 = resulting->head;
        write (out, "\n-RESULT CONFIG--\n", 18);

        while (temp2) {
            write (out, temp2->profile, strlen (temp2->profile));
            temp2 = temp2->next;
        };
    } else {
        write_log (2, "TELNET_MOD: [%s] switch don't need any updates", host_addr);
        sprintf (query,
                 "UPDATE switches SET last_update=(date_trunc( 'second' , 'now'::timestamp )), last_state=2 WHERE sw_ip='%s';",
                 host_addr);

    }
    /*
     *
     * UPDATE PROCEDURE
     *
     */
    timeo.tv_sec = 30;
    timeo.tv_usec = 0;

    write (out, delimt, 18);
    write (out, "# UPDATING SWITCH #\n", 20);

    // send (sock, "\n\n", 2, 0);

    int counter = 0;
    int timeouts = 0;
    if (resulting) {
        write_log (1, "TELNET_MOD: [%s] acl_update: trying to update switch", host_addr);
        temp2 = resulting->head;

        timeo.tv_sec = 5;
        timeo.tv_usec = 0;
        FD_ZERO (&rd);
        FD_SET (sock, &rd);

        if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0)
            send (sock, "\n\n", 2, 0);

        while (temp2) {
            if (timeouts > 10) {
                write_log (1,
                           "TELNET_MOD: [%s] acl_update: 10+ timeout in update, i'll give up",
                           host_addr);
                is_error = 1;
                goto exit_label;
            }

            memset (buffer, '\0', BUFF);

            timeo.tv_sec = 3;
            timeo.tv_usec = 0;
            FD_ZERO (&rd);
            FD_SET (sock, &rd);
            /*
             * Here we send updates to switch
             */
            if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0)
                send (sock, temp2->profile, strlen (temp2->profile), 0);
            else {
                timeouts++;
                write_log (1, "TELNET_MOD: [%s] acl_update: timeout on send(updates)",
                           host_addr);
                /*
                 * Just try again
                 */
				timeo.tv_sec = 3;
    			timeo.tv_usec = 0;
    			FD_ZERO (&rd);
            	FD_SET (sock, &rd);
                if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0)
                    send (sock, temp2->profile, strlen (temp2->profile), 0);
            };

            do {
                FD_ZERO (&rd);
                FD_SET (sock, &rd);
                timeo.tv_sec = 8;
                timeo.tv_usec = 0;
                if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0) {
                    count = recv (sock, buffer, BUFF, 0);
                    write (out, buffer, count);
                    // fflush(out);
                } else {
                    timeouts++;
                    break;
                    write_log(1, "TELNET_MOD: [%s] acl_update: timeout on recv()", host_addr);
                }
            } while (!memmem (buffer, count, delimiter, delimiter_size));

            temp2 = temp2->next;
            counter++;
        }
        if (is_error == 1)
            sprintf (query,
                     "UPDATE switches SET last_update=(date_trunc( 'second' , 'now'::timestamp )), last_state=0 WHERE sw_ip='%s';",
                     host_addr);
        else
            sprintf (query,
                     "UPDATE switches SET last_update=(date_trunc( 'second' , 'now'::timestamp )), last_state=1 WHERE sw_ip='%s';",
                     host_addr);
    }
exit_label:
    if (resulting) {
        temp2 = resulting->head;
        pdb_destroy (&temp2);
    }

    write (out, delimt, 18);
    write (out, "# LOGOUT   SWITCH #\n", 20);
    if (memmem (buffer, count, delimiter, delimiter_size)) {
        memset (buffer, '\0', sizeof buffer);

        timeo.tv_sec = 3;
        timeo.tv_usec = 0;
        FD_ZERO (&rd);
        FD_SET (sock, &rd);
        if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0)
            send (sock, "logout\n", 7, 0);
        do {
            timeo.tv_sec = 3;
            timeo.tv_usec = 0;
            FD_ZERO (&rd);
            FD_SET (sock, &rd);
            if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0)
                count = recv (sock, buffer, BUFF, 0);
            else
                break;
        } while (!strstr (buffer, "Logout") && count);
    };
    write_log (1,
               "TELNET_MOD: [%s] acl_update: switch updated (%d lines transferred)",
               host_addr, counter);
    write (out, "# CLOSE ALL FILES #\n", 20);

    write_log (3, "TELNET_MOD: [%s] acl_update: ended", host_addr);

	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};


    PQclear (result);
abandon:
    close (sock);
    close (out);

    return 0;

};

inline char *
mac_refine (register char *mac) {
    char *ptr = mac;
    // *ptr = toupper (*ptr);
    while (*ptr) {
        if (*ptr == '-')
            *ptr = ':';
        else
            *ptr = tolower (*ptr);
        (void) *ptr++;
    };

    return mac;

};

fdb
fdb_parse (char *str, int swt) {
    pcre *f;			/* переменная для хранения
				 * преобразованного шаблона */
    pcre_extra *f_ext;		/* переменная для хранения
				 * дополнительных данных */
    char *pattern =
        "(\\d+)\\s+[[:alnum:]]+\\s+([0-9A-F]+-[0-9A-F]+-[0-9A-F]+-[0-9A-F]+-[0-9A-F]+-[0-9A-F]+)\\s{1,3}(\\d+)";
    // char *pattern_3028 =
    // "(\\d+)\\s+[[:alnum:]]+\\s+([0-9A-F]+-[0-9A-F]+-[0-9A-F]+-[0-9A-F]+-[0-9A-F]+-[0-9A-F]+)\\s{1,3}(\\d+)";
    char *pattern_3010 = "(\\d+)\\s+\\S+\\s+(\\S+)\\s+(\\d+)\\s+\\S+\\R";
    const char *errstr;		/* буфер для сообщения об ошибке */
    int errchar;		/* номер символа */
    int vector[1000];		/* массив для результатов */
    int vecsize = 1000;		/* размер массива */
    int pairs = 0;		/* количество найденных пар */
    const char *vid;		/* для функции pcre_get_substring */
    const char *port;
    const char *mac;
    // const unsigned char *tables; /* буфер для хранения таблицы
    // * * символов */
    int j;
    fdb fdb_list = NULL;

    // setlocale (LC_ALL, "ru_RU.UTF-8");
    // tables = pcre_maketables ();

    write_log (2, "TELNET_MOD:Start fdb parse");
    if (swt == DES3010) {
        if ((f =
                    pcre_compile (pattern_3010, PCRE_CASELESS | PCRE_MULTILINE, &errstr,
                                  &errchar, NULL)) == NULL) {
            write_log (2, "TELNET_MOD: pcre pattern error %s\nSymb N%i\nPattern:%s\n",
                       errstr, errchar, pattern_3010);
            return NULL;
        }
    } else
        if ((f =
                    pcre_compile (pattern, PCRE_CASELESS | PCRE_MULTILINE, &errstr, &errchar,
                                  NULL)) == NULL) {
            write_log (2, "TELNET_MOD: pcre pattern error %s\nSymb N%i\nPattern:%s\n", errstr,
                       errchar, pattern);
            return NULL;
        }

    f_ext = pcre_study (f, 0, &errstr);

    j = 0;
    while ((pairs =
                pcre_exec (f, f_ext, str, strlen (str), j, PCRE_NOTEMPTY,
                           vector, vecsize)) >= 0) {

        pcre_get_substring (str, vector, pairs, 1, &vid);
        pcre_get_substring (str, vector, pairs, 2, &mac);
        pcre_get_substring (str, vector, pairs, 3, &port);
        fdb_insert (&fdb_list, mac_refine ((char *) mac), atoi (port), atoi (vid));
        pcre_free_substring (vid);
        pcre_free_substring (mac);
        pcre_free_substring (port);
        j = vector[1] + 1;
    }

    write_log (2, "TELNET_MOD:End fdb parse");
    free (f_ext);
    pcre_free (f);
    // free ((void *) tables);
    return fdb_list;
}

extern int
    fdb_get2 (char *host_addr, char *login, char *pass, char *delimiter, int swtype,
              PGconn * resurs, char *copy) {

    struct sockaddr_in address;
    struct in_addr inaddr;
    struct timeval timeo;
    struct timeval socktime;
    int sock;
    int cnt;
    int lag_count = 0;
    int out1;
    // char *copy;
    char quer[20];		// show fdb port 26\n
    char query[256];
    char filename[64];
    char buffer[BUFF];
    fd_set rd;
    char say[5];

    int delimiter_size = strlen (delimiter);

    PGresult *result;

    extern opt *opts;
    char errbuf[256];

    // copy = my_malloc (opts->malloc_size);

    write_log (2, "TELNET_MOD: [%s] fdb_get2() started", host_addr);

    sprintf (filename, "%s/%s.fdb", opts->sw_log_dir, host_addr);

    if ((out1 = open (filename, O_WRONLY | O_CREAT | O_TRUNC | O_NONBLOCK, (mode_t) 0666)) < 0)
        //
        // SELECT distinct sw_port from fdb_ports where sw_ip='10.1.0.25';
        //

        memset (buffer, '\0', BUFF);
    if ((sock = socket (PF_INET, SOCK_STREAM, 0)) < 0) {
        strerror_r (errno, errbuf, 256);
        write_log (1, "(!) Socket failed: %s", errbuf);
        // free(copy);
        close (out1);
        return (11);
    };

    socktime.tv_sec = 30;
    socktime.tv_usec = 0;
    setsockopt (sock, SOL_SOCKET, SO_SNDTIMEO, &socktime, sizeof (socktime));

    socktime.tv_sec = 30;
    socktime.tv_usec = 0;
    setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, &socktime, sizeof (socktime));

    fcntl(sock, F_SETOWN, getpid());

	signal (SIGPIPE, s_handler_bulk);
	signal (SIGIO, s_handler_bulk);
	signal (SIGURG, s_handler_bulk);


    address.sin_family = AF_INET;
    address.sin_port = htons (23);

    inet_aton (host_addr, &inaddr);
    memcpy (&address.sin_addr, &inaddr.s_addr, sizeof (address.sin_addr));

    if (connect (sock, (struct sockaddr *) &address, sizeof (address))) {
        strerror_r (errno, errbuf, 256);
        write_log (1, "(!) Connection to %s failed: %s", host_addr, errbuf);
        // free (copy);
        close (out1);
        close (sock);
        return errno;
    };
    size_t count;

    timeo.tv_sec = 10;
    timeo.tv_usec = 0;

    FD_ZERO (&rd);
    FD_SET (sock, &rd);
    // dprintf(out1,"\n PASS: '%s' \t Login: '%s' \n", pass, login);
    do {
        FD_ZERO (&rd);
        FD_SET (sock, &rd);
        timeo.tv_sec = 5;
        timeo.tv_usec = 0;
        if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0) {
            count = recv (sock, buffer, BUFF, 0);
            write (out1, buffer, count);
            if (strstr (buffer, "Fail!")) {
                write_log (1, "TELNET_MOD: (!) [%s] Login to switch failed", host_addr);
                close (sock);
                close (out1);
                // PQfinish(resurs);
                return 11;
            };
            if (strstr (buffer, "ame:")) {
                // sleep(1);

                FD_ZERO (&rd);
                FD_SET (sock, &rd);
                timeo.tv_sec = 3;
                timeo.tv_usec = 0;
                if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0)
                    send (sock, login, strlen (login), 0);
                else {
                    write_log (1, "TELNET_MOD: (!) [%s] Timeout in send (login)",
                               host_addr);
                    close (sock);
                    close (out1);
                    return 11;
                };

                memset (buffer, '\0', BUFF);
                write_log (3, "TELNET_MOD: (!) [%s] User name send", host_addr);

            };
            if (strstr (buffer, "ord:")) {
                // sleep(1);
                FD_ZERO (&rd);
                FD_SET (sock, &rd);
                timeo.tv_sec = 3;
                timeo.tv_usec = 0;
                if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0)
                    send (sock, pass, strlen (pass), 0);
                else {
                    write_log (1, "TELNET_MOD: (!) [%s] Timeout in send (password)",
                               host_addr);
                    close (sock);
                    close (out1);
                    return 11;
                };

                memset (buffer, '\0', BUFF);
                write_log (3, "TELNET_MOD: (!) [%s] Password send", host_addr);
            };
        } else {
            write_log (1, "TELNET_MOD: [%s] (!) fdb_get() Timeout in login", host_addr);
            goto throw_out2;
        };
    } while (!strstr (buffer, delimiter) && count > 0);

    memset (buffer, '\0', BUFF);
    /*
     * if (swtype != DES3028) send (sock, "disable clipaging\n", 18, 0); else
     */
    FD_ZERO (&rd);
    FD_SET (sock, &rd);
    timeo.tv_sec = 3;
    timeo.tv_usec = 0;

    if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0)
        send (sock, "enable clipaging\n", 17, 0);
    else
        write_log (1, "TELNET_MOD: [%s] fdb_get(%d): timeout on send (en clip)",
                   host_addr, __LINE__);

    FD_ZERO (&rd);
    FD_SET (sock, &rd);
    timeo.tv_sec = 5;
    timeo.tv_usec = 0;

    if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0)
        while ((count = recv (sock, buffer, BUFF, 0) > 0)
                && (!strstr (buffer, delimiter)));
    else
        write_log (1, "TELNET_MOD: fdb_get(%d): Strange timeout", __LINE__);

    memset (query, 0, 256);
    sprintf (query, "SELECT DISTINCT sw_port FROM fdb_ports WHERE sw_ip='%s';",
             host_addr);
             
	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};
             
    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};


    for (cnt = 0; cnt < PQntuples (result); cnt++) {
        // usleep(1000);
        memset (buffer, '\0', BUFF);
        memset (quer, '\0', 20);
        sprintf (quer, "show fdb port %s\n", PQgetvalue (result, cnt, 0));

        FD_ZERO (&rd);
        FD_SET (sock, &rd);
        timeo.tv_sec = 10;
        timeo.tv_usec = 0;

        if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0) {
            send (sock, quer, 20, 0);
        } else {
            write_log (1, "TELNET_MOD: [%s] timeout on send(query)", host_addr);
        };

        do {
            if (lag_count > 4) {
                write_log (1, "TELNET_MOD: [%s] fdb_get(): Giving up, too many timeouts",
                           host_addr);
                goto throw_out;
            }

            memset (buffer, '\0', BUFF);

            timeo.tv_sec = 30;
            timeo.tv_usec = 0;
            FD_ZERO (&rd);
            FD_SET (sock, &rd);

            if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0) {
                count = 0;
                count = recv (sock, buffer, BUFF, 0);
				if (count > BUFF) write_log (1, "(!) TELNET_MOD: [%s] recv() returned something wrong!",host_addr);
				else catstring (copy, buffer, count);
                write (out1, buffer, count);
                if (strstr (buffer, "+C")) {	// if we have clipping mode,
                    timeo.tv_sec = 5;
                    timeo.tv_usec = 0;
                    FD_ZERO (&rd);
                    FD_SET (sock, &rd);
                    if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0) {
                        send (sock, " ", 1, 0);	// we should send 'n' for next page
                    } else {
                        write (out1, "[X]", 3);
                    };
                };

            } else {
                lag_count++;
                strerror_r (errno, errbuf, 256);
                write_log (1, "TELNET_MOD: [%s] fdb_get(%d): "
                           "Error in select(%d sec left): %s", host_addr, __LINE__,
                           timeo.tv_sec, errbuf);
                break;
            };
        } while (!memmem (buffer, count, delimiter, delimiter_size)
                 && !strstr (buffer, "ies :") && count > 0);
    }
    memset (buffer, '\0', BUFF);
    timeo.tv_sec = 5;
    timeo.tv_usec = 0;
    FD_ZERO (&rd);
    FD_SET (sock, &rd);
    if (select (sock + 1, NULL, &rd, &rd, &timeo) > 0)
        if (swtype == DES3028) send (sock, "\nclear flood\n", 13, 0);
        send (sock, "\nlogout\n", 8, 0);
        
        
    do {

        FD_ZERO (&rd);
        FD_SET (sock, &rd);
        timeo.tv_sec = 10;
        timeo.tv_usec = 0;
        if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0) {
            count = recv (sock, buffer, BUFF, 0);
            write (out1, buffer, count);
            sprintf (say, "+%d+", count);
            write (out1, say, 5);
            catstring (copy, buffer, count);
        } else
            break;

    } while (!strstr (buffer, "Logout") && count > 0);
    // }

    // write_log (1, "logouted");
    write_log (3, "TELNET_MOD: [%s] (debug) fdb lenght: %d", host_addr, strlen (copy));
    fdb fdb_x;
    PQclear (result);
    write (out1, "\n--\n", 4);
    write (out1, copy, strlen (copy));
    fdb_x = fdb_parse (copy, swtype);
    // (*fdb_out) = fdb_x;

    if (fdb_x != NULL) {
        save_fdb (fdb_x, host_addr, resurs);
        fdb_destroy (&fdb_x);
    } else
        write_log (1, "TELNET_MOD: [%s] (!) Zero-sized fdb-list after fdb_parse",
                   host_addr);

    memset (query, '\0', 256);
    sprintf (query,
             "UPDATE switches SET last_connect=(date_trunc( 'second' , 'now'::timestamp )) WHERE sw_ip='%s';",
             host_addr);

	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};
    
    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

throw_out:
    PQclear (result);
throw_out2:
    // PQfinish (resurs);
    // free (copy);
    close (sock);
    close (out1);
    write_log (2, "TELNET_MOD: [%s] fdb_get() stopping", host_addr);

    return 0;
}

int
impb3010_update (char *host_addr, char *login, char *pass, char *delimiter,
                 PGconn * resurs, char *copy) {
    int timeouts = 0;
    struct sockaddr_in address;
    struct in_addr inaddr;
    struct timeval timeo;
    int sock;
    int out;
    fd_set rd;
    char buffer[BUFF];
    size_t count;
    char filename[32];
    pdb sw_data = NULL;
    pdb db_data = NULL;
    pdb resulting = NULL;
    pdb sw_prof = NULL;
    pdb db_prof = NULL;
    pdb tmp = NULL;

    extern opt *opts;

    char errbuf[256];

    write_log (2, "TELNET_MOD: [%s] impb3010_update() started", host_addr);

    sprintf (filename, "%s/%s.impb.log", opts->sw_log_dir, host_addr);
    if ((out = open (filename, O_WRONLY | O_CREAT | O_TRUNC, (mode_t) 0666)) < 0)
        perror ("open");

    if ((sock = socket (PF_INET, SOCK_STREAM, 0)) < 0) {
        strerror_r (errno, errbuf, 256);
        write_log (1, "(!) Socket failed: %s", errbuf);
        close (out);

        return (11);
    };

    timeo.tv_sec = 10;
    timeo.tv_usec = 0;
    setsockopt (sock, SOL_SOCKET, SO_SNDTIMEO, &timeo, sizeof (timeo));
    timeo.tv_sec = 10;
    timeo.tv_usec = 0;
    setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, &timeo, sizeof (timeo));

    address.sin_family = AF_INET;
    address.sin_port = htons (23);

    inet_aton (host_addr, &inaddr);
    memcpy (&address.sin_addr, &inaddr.s_addr, sizeof (address.sin_addr));
    if (connect (sock, (struct sockaddr *) &address, sizeof (address))) {
        strerror_r (errno, errbuf, 256);
        write_log (1,
                   "TELNET_MOD: impb3010_update: (!) Connection to %s failed: %s",
                   host_addr, errbuf);
        close (out);
        close (sock);
        return errno;
    };

    memset (buffer, 0, BUFF);

    FD_ZERO (&rd);
    FD_SET (sock, &rd);

    do {
        if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0) {
            count = recv (sock, buffer, BUFF, 0);
            write (out, buffer, count);
            if (strstr (buffer, "Fail!")) {
                write_log (1,
                           "TELNET_MOD: (!) impb3010_update(): [%s] Login to switch failed",
                           host_addr);
                close (sock);
                close (out);
                return 11;
            };
            if (strstr (buffer, "ame:")) {
                send (sock, login, strlen (login), 0);
                memset (buffer, 0, strlen (buffer));
                write_log (2, "TELNET_MOD: [%s] User name send", host_addr);
            };
            if (strstr (buffer, "ord:")) {
                send (sock, pass, strlen (pass), 0);
                memset (buffer, 0, strlen (buffer));
                write_log (2, "TELNET_MOD: [%s] Password send", host_addr);
            };
        } else {
            timeouts++;
            write_log (1, "TELNET_MOD: [%s] impb3010_update() Timeout in login",
                       host_addr);
            break;
        };
    } while (!strstr (buffer, delimiter) && count >= 0);

    db_data = create_bindings (host_addr, &db_prof, resurs);

    memset (buffer, 0, sizeof (buffer));
    send (sock, "disable clipaging\n", 18, 0);

    if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0)
        while ((count = recv (sock, buffer, BUFF, 0)) > 0 && !strstr (buffer, delimiter));
    else
        write_log (1, "TELNET_MOD: [%s] impb3010_update() Timeout in action", host_addr);

    if (strstr (buffer, delimiter)) {
        memset (buffer, '\0', sizeof buffer);
        send (sock, "show config current_config\n", 27, 0);
        timeo.tv_sec = 10;
        timeo.tv_usec = 0;
        timeouts = 0;
        do {
            // if (timeouts>10)
            if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0) {
                count = recv (sock, buffer, BUFF, 0);
                write (out, buffer, count);
                if (count <= 0)
                    break;
                /*
                 * if (!(copy = my_realloc (copy, strlen (copy) + strlen (buffer) + 2)))
                 * { write_log (1, "TELNET_MOD: (!) acl_update: Memory exhausted,
                 * my_realloc failed. Bug?"); return -1; }
                 */
                strncat (copy, buffer, count);

            } else {
                timeouts++;
                break;
            }
        } while (!strstr (buffer, delimiter));

    };

    timeo.tv_sec = 10;
    timeo.tv_usec = 0;
    if (strstr (buffer, delimiter)) {
        memset (buffer, '\0', sizeof buffer);
        send (sock, "show address_binding ip_mac\n", 28, 0);

        do {
            if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0) {
                count = recv (sock, buffer, BUFF, 0);
                write (out, buffer, count);
                if (count <= 0)
                    break;
                /*
                 * if (!(copy2 = my_realloc (copy2, strlen (copy2) + strlen (buffer) +
                 * 2))) { write_log (1, "TELNET_MOD: (!) acl_update: Memory exhausted,
                 * my_realloc failed. Bug?"); return -1; }
                 */
                strncat (copy, buffer, count);
            } else
                break;
        } while (!strstr (buffer, delimiter));

    };

    memset (buffer, '\0', sizeof buffer);

    sw_data = parse_3010_cfg (copy, &sw_prof);
    dprintf (out, "\nSwitch config:-------------------------------------\n");
    if (sw_data) {
        tmp = sw_data->head;
        while (tmp) {
            dprintf (out, "sw rule: %s", tmp->profile);
            tmp = tmp->next;
        }
    }
    dprintf (out, "\nDatabase config:-----------------------------------\n");
    if (db_data) {
        tmp = db_data->head;
        while (tmp) {
            dprintf (out, "db rule: %s", tmp->profile);
            tmp = tmp->next;
        }
    }
    resulting = impb_compare (&sw_prof, &db_prof, &sw_data, &db_data, out);

    dprintf (out, "\nResulting config:----------------------------------\n");
    if (resulting) {
        tmp = resulting->head;
        while (tmp) {
            write (out, tmp->profile, strlen (tmp->profile));
            tmp = tmp->next;
        };
    };
    dprintf (out, "\n----------------------Updating---------------------\n");
    int counter = 0;
    if (resulting) {
        write_log (2, "TELNET_MOD: [%s] impb_update: trying to update switch", host_addr);
        tmp = resulting->head;
        while (tmp) {
            memset (buffer, '\0', sizeof buffer);
            send (sock, tmp->profile, strlen (tmp->profile), 0);

            do {
                if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0) {
                    count = recv (sock, buffer, BUFF, 0);
                    write (out, buffer, count);
                } else
                    break;
            } while (!strstr (buffer, delimiter) && count);

            tmp = tmp->next;
            counter++;
        }

        if (resulting) {
            tmp = resulting->head;
            pdb_destroy (&tmp);
        }
    } else
        write_log (1, "TELNET_MOD: [%s] impb_update(): switch don't need updates",
                   host_addr);

    if (strstr (buffer, delimiter)) {
        memset (buffer, '\0', sizeof buffer);
        send (sock, "logout\n", 7, 0);
        do {
            if (select (sock + 1, &rd, NULL, &rd, &timeo) > 0) {
                count = recv (sock, buffer, BUFF, 0);
                write (out, buffer, count);
            } else
                break;
        } while (!strstr (buffer, "Logout") && count);
    };
    write_log (1,
               "TELNET_MOD: [%s] impb_update: switch updated (%d lines transferred)",
               host_addr, counter);

    write_log (1, "TELNET_MOD: [%s] impb_update: ended", host_addr);
    close (sock);
    close (out);
    return 0;
};

pdb
parse_3010_cfg (char *cfg, pdb * pr) {
    pcre *f;
    // char *cfg
    pcre_extra *f_ext;
    /*
     * IP Address MAC Address Ports * --------------- ----------------- ------------- *
     * 62.33.247.139 00-04-AC-E8-34-08 1
     */

    char *pattern = "(\\d+\\.\\d+\\.\\d+\\.\\d+)\\s+(..-..-..-..-..-..)\\s\\s(\\d)";

    char *prof_pat =
        "(config\\saddress_binding\\sip_mac\\sports\\s\\d+\\sstate\\senable\\sallow_zeroip\\senable\\n)";
    const char *errstr;
    int errchar;
    int vector[10000];
    int vecsize = 10000;
    int pairs = 0;

    char subj[512];
    const char *prof;
    const char *prof2;
    const char *prof3;

    // const unsigned char *tables;
    int j;
    pdb prof_list = NULL;
    pdb pr_list = NULL;

    // tables = pcre_maketables ();

    write_log (2, "TELNET_MOD:Start configuration parse");

    if ((f =
                pcre_compile (prof_pat, PCRE_CASELESS | PCRE_MULTILINE, &errstr, &errchar,
                              NULL)) == NULL) {
        write_log (2, "TELNET_MOD: pcre pattern error %s\nSymb N%i\nPattern:%s\n", errstr,
                   errchar, prof_pat);
    } else {

        f_ext = pcre_study (f, 0, &errstr);
        j = 0;
        while ((pairs =
                    pcre_exec (f, f_ext, cfg, strlen (cfg), j, PCRE_NOTEMPTY,
                               vector, vecsize)) >= 0) {
            pcre_get_substring (cfg, vector, pairs, 1, &prof);
            pdb_insert (&pr_list, (char *) prof);
            pcre_free_substring (prof);
            j = vector[1] + 1;
        }
    }

    pcre_free (f);

    if ((f =
                pcre_compile (pattern, PCRE_CASELESS | PCRE_MULTILINE, &errstr, &errchar,
                              NULL)) == NULL) {
        write_log (2, "TELNET_MOD: pcre pattern error %s\nSymb N%i\nPattern:%s\n", errstr,
                   errchar, pattern);
    } else {

        f_ext = pcre_study (f, 0, &errstr);
        j = 0;
        while ((pairs =
                    pcre_exec (f, f_ext, cfg, strlen (cfg), j, PCRE_NOTEMPTY,
                               vector, vecsize)) >= 0) {
            memset (subj, '\0', 512);
            pcre_get_substring (cfg, vector, pairs, 1, &prof);
            pcre_get_substring (cfg, vector, pairs, 2, &prof2);
            pcre_get_substring (cfg, vector, pairs, 3, &prof3);
            sprintf (subj,
                     "create address_binding ip_mac ipaddr %s mac_address %s ports %s\n",
                     prof, prof2, prof3);
            pdb_insert (&prof_list, subj);
            pcre_free_substring (prof);
            pcre_free_substring (prof2);
            pcre_free_substring (prof3);
            j = vector[1] + 1;
        }
    }

    write_log (2, "TELNET_MOD:End configuration parse");
    free (f_ext);
    pcre_free (f);
    // free ((void *) tables);
    (*pr) = pr_list;
    return prof_list;
};
