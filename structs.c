
/***************************************************************************
 *   Copyright (C) 2008 by Denis V. Meltsaykin   			   *
 *   dvm.avgoor@gmail.com   						   *
 *                                                                         *
 *   Permission is hereby granted, free of charge, to any person obtaining *
 *   a copy of this software and associated documentation files (the       *
 *   "Software"), to deal in the Software without restriction, including   *
 *   without limitation the rights to use, copy, modify, merge, publish,   *
 *   distribute, sublicense, and/or sell copies of the Software, and to    *
 *   permit persons to whom the Software is furnished to do so, subject to *
 *   the following conditions:                                             *
 *                                                                         *
 *   The above copyright notice and this permission notice shall be        *
 *   included in all copies or substantial portions of the Software.       *
 *                                                                         *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       *
 *   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    *
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*
 *   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR     *
 *   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, *
 *   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR *
 *   OTHER DEALINGS IN THE SOFTWARE.                                       *
 ***************************************************************************/

#include "nms.h"

extern int
    queue_push (q_un * que, int cmd_id, short cmd_type,
                short cmd_priority, short timeslice, char *target, char *delimiter,
                char *stype) {
    q_un temp;
    if (!*que) {
        (*que) = malloc (sizeof **que);
        (*que)->head = *que;
        (*que)->next = NULL;
        (*que)->prev = NULL;
        (*que)->count = 1;
        (*que)->lock_state = NOTLOCKED;
        (*que)->cmd_id = cmd_id;
        (*que)->cmd_type = cmd_type;
        (*que)->cmd_priority = cmd_priority;
        (*que)->timeslice = timeslice;
        (*que)->thread_id = 0;
        (*que)->delimiter = strdup (delimiter);
        (*que)->target = strdup (target);
        if (strstr (stype, "DES-3526"))
            (*que)->type = DES3526;
        if (strstr (stype, "DES-3550"))
            (*que)->type = DES3550;
        if (strstr (stype, "DES-3010"))
            (*que)->type = DES3010;
        if (strstr (stype, "DES-3028"))
            (*que)->type = DES3028;

    } else {
        temp = malloc (sizeof *temp);
        temp->head = (*que)->head;
        temp->next = NULL;
        temp->prev = (*que);
        temp->count = (*que)->count + 1;
        temp->cmd_id = cmd_id;
        temp->lock_state = NOTLOCKED;
        temp->cmd_type = cmd_type;
        temp->thread_id = 0;
        temp->cmd_priority = cmd_priority;
        temp->timeslice = timeslice;
        temp->delimiter = strdup (delimiter);
        temp->target = strdup (target);
        if (strstr (stype, "DES-3526"))
            temp->type = DES3526;
        if (strstr (stype, "DES-3550"))
            temp->type = DES3550;
        if (strstr (stype, "DES-3010"))
            temp->type = DES3010;
        if (strstr (stype, "DES-3028"))
            temp->type = DES3028;
        (*que)->next = temp;
        (*que) = temp;
    };
    return 0;
};

extern int
    queue_destroy (q_un * list) {
    q_un next;
    *list = (*list)->head;
    while (*list) {
        next = (*list);
        *list = (*list)->next;
        free (next->target);
        free (next->delimiter);
        free (next);
    };
    *list = NULL;
    return 0;
};

extern int
    fdb_insert (fdb * list, char *mac, int port, int vlan) {
    fdb temp;
    if (*list == NULL) {
        (*list) = malloc (sizeof **list);
        (*list)->head = *list;
        (*list)->next = NULL;
        (*list)->port = port;
        (*list)->vlan = vlan;
        (*list)->mac = strdup (mac);
    } else {
        temp = malloc (sizeof *temp);
        temp->head = (*list)->head;
        temp->next = NULL;
        temp->port = port;
        temp->vlan = vlan;
        temp->mac = strdup (mac);
        (*list)->next = temp;
        (*list) = temp;
    };
    return 0;
};

extern int
    fdb_destroy (fdb * list) {
    fdb next;
    *list = (*list)->head;
    while (*list) {
        next = (*list);
        *list = (*list)->next;
        free (next->mac);
        free (next);
    };
    *list = NULL;
    return 0;
};

extern int
    rdb_insert (rdb * list, char *string, int port, int access_id, int profile_id, int action) {
    rdb temp;
    if (*list == NULL && string) {
        (*list) = malloc (sizeof (struct _rdb));
        (*list)->head = *list;
        (*list)->next = NULL;
        (*list)->prev = NULL;
        (*list)->string = strdup (string);
        // memset((*list)->string, 0, sizeof ((*list)->string));
        // memcpy((*list)->string, string, strlen(string));
        (*list)->action = action;
        (*list)->port = port;
        (*list)->access_id = access_id;
        (*list)->profile_id = profile_id;
    } else if (string) {
        temp = malloc (sizeof (struct _rdb));
        temp->head = (*list)->head;
        temp->next = NULL;
        temp->prev = (*list);
        temp->string = strdup (string);
        // memset(temp->string, 0, sizeof (temp->string));
        // memcpy(temp->string, string, strlen(string));
        temp->action = action;
        temp->port = port;
        temp->access_id = access_id;
        temp->profile_id = profile_id;
        (*list)->next = temp;
        (*list) = temp;
    };
    return 0;
};

/*
 * extern int rdb_destroy (rdb* list) { rdb next; *list=(*list)->head;
 * while (*list) { next=(*list); (*list)=(*list)->next;
 * free(next->string); free(next); };
 *
 * free(*list); return 0; };
 */
extern int
    rdb_destroy (rdb * list) {
    if (*list) {
        rdb next;
        (*list) = (*list)->head;
        while (*list) {
            next = (*list);
            (*list) = (*list)->next;
            free (next->string);
            free (next);
        };

        // free (*list);
    };
    return 0;
};

extern int
    rdb_delete (rdb * t_list) {
    rdb t_next;
    rdb t_prev;
    rdb tmp;
    rdb marker;

    if (*t_list && !(*t_list)->prev && !(*t_list)->next) {
        // (*t_list)->head = NULL;
        free ((*t_list)->string);
        free ((*t_list));
        (*t_list) = NULL;
        t_list = NULL;
        return -1;
    };

    //
    //
    // If we have all elements in list, i.e. this is middle element
    //
    //

    if (*t_list && (*t_list)->next && (*t_list)->prev) {
        tmp = (*t_list);
        t_next = (*t_list)->next;
        t_prev = (*t_list)->prev;
        t_next->prev = t_prev;
        t_prev->next = t_next;
        (*t_list) = t_next;
        free (tmp->string);
        free (tmp);
        return 1;
    };

    //
    //
    // If we haven't next element in list, i.e. this is last element
    //
    //
    if (*t_list && (*t_list)->prev && !(*t_list)->next) {
        t_next = NULL;
        tmp = (*t_list);
        t_prev = (*t_list)->prev;
        t_prev->next = NULL;
        // (*t_list) = t_prev;
        (*t_list) = NULL;
        free (tmp->string);
        free (tmp);
        return 2;
    };

    //
    //
    // If we haven't prev element in list, i.e. this is first element
    //
    //
    if (*t_list && (*t_list)->next && !(*t_list)->prev) {
        t_prev = NULL;
        tmp = (*t_list);
        (*t_list) = (*t_list)->next;
        (*t_list)->prev = NULL;
        (*t_list)->head = (*t_list);
        tmp->head = (*t_list);
        //
        //
        // Running through all the list, need to change list->head
        // pointer!
        //
        //
        marker = (*t_list);
        while ((marker = marker->next) != NULL)
            marker->head = (*t_list);

        // if (tmp->string)
        free (tmp->string);
        free (tmp);
        return 3;
    };

    //
    //
    // If we haven't prev and next elements in list, i.e. this is single
    // element
    //
    //
    return 0;
};

extern int
    pdb_destroy (pdb * list) {
    pdb prev;
    if (*list != NULL) {
        (*list) = (*list)->head;
        while (*list) {
            prev = (*list);
            (*list) = (*list)->next;
            free (prev->profile);
            free (prev);
        };
        // free (*list);
    };
    // *list = NULL;
    return 0;
};

extern int
    pdb_delete (pdb * t_list) {
    pdb t_next;
    pdb t_prev;
    pdb tmp;
    pdb marker;

    //
    //
    // If we haven't prev and next elements in list, i.e. this is single
    // element
    //
    //

    if (*t_list && !(*t_list)->prev && !(*t_list)->next) {
        free ((*t_list)->profile);
        free ((*t_list));
        (*t_list) = NULL;
        return -1;
    };

    //
    //
    // If we have all elements in list, i.e. this is middle element
    //
    //

    if (*t_list && (*t_list)->next && (*t_list)->prev) {
        tmp = (*t_list);
        t_next = (*t_list)->next;
        t_prev = (*t_list)->prev;
        t_next->prev = t_prev;
        (*t_list) = t_next;
        t_prev->next = t_next;
        free (tmp->profile);
        // tmp->profile=NULL;
        free (tmp);
        // tmp=NULL;
        return 1;
    };

    //
    //
    // If we haven't next element in list, i.e. this is last element
    //
    //
    if (*t_list && (*t_list)->prev && !(*t_list)->next) {
        t_next = NULL;
        tmp = (*t_list);
        t_prev = (*t_list)->prev;
        t_prev->next = NULL;
        (*t_list) = NULL;
        free (tmp->profile);
        free (tmp);
        return 2;
    };

    //
    //
    // If we haven't prev element in list, i.e. this is first element
    //
    //
    if (*t_list && (*t_list)->next && !(*t_list)->prev) {
        t_prev = NULL;
        tmp = (*t_list);
        (*t_list) = (*t_list)->next;
        (*t_list)->prev = NULL;
        (*t_list)->head = (*t_list);
        //
        //
        // Running through all the list, need to change list->head
        // pointer!
        //
        //
        marker = (*t_list);
        while ((marker = marker->next) != NULL)
            marker->head = (*t_list);

        free (tmp->profile);
        free (tmp);
        return 3;

    };

    return 0;
};

extern int
    pdb_insert (pdb * list, char *profile) {
    pdb temp;
    if (*list == NULL && profile) {
        (*list) = malloc (sizeof (struct _pdb));
        (*list)->head = (*list);
        (*list)->next = NULL;
        (*list)->prev = NULL;
        (*list)->profile = strdup (profile);
        // memset((*list)->profile, 0, sizeof ((*list)->profile));
        // memcpy((*list)->profile, profile, strlen(profile));
    } else if (profile) {
        temp = malloc (sizeof (struct _pdb));
        temp->head = (*list)->head;
        temp->next = NULL;
        temp->prev = (*list);
        temp->profile = strdup (profile);
        // memset(temp->profile, 0, sizeof (temp->profile));
        // memcpy(temp->profile, profile, strlen(profile));
        (*list)->next = temp;
        (*list) = temp;
    };
    return 0;
};

extern int
    opts_free (opt * a) {
    free (a->dbname);
    free (a->dbhost);
    free (a->dbpass);
    free (a->dblogin);
    free (a->sw_delimiter);
    free (a->swlogin);
    free (a->swpass);
    free (a);
    return 0;
};

extern int
    dev_islocked (char *ip, _dev_lock * temp) {
    static pthread_mutex_t dev_mtx;
    pthread_mutex_lock (&dev_mtx);
    // FIND IP IN _dev_lock
    if (*temp != NULL) {
        _dev_lock device = (*temp)->head;
        while (device) {
            if (strcmp (device->name, ip) == 0) {
                pthread_mutex_unlock (&dev_mtx);
                return 1;
            };
            device = device->next;
        };
        pthread_mutex_unlock (&dev_mtx);
        return 0;
    } else {
        pthread_mutex_unlock (&dev_mtx);
        return 0;
    };
};

extern int
    dev_lockup (char *ip, _dev_lock * temp) {
    // CREATE ENTRY IN _dev_lock
    static pthread_mutex_t dev_mtx;
    _dev_lock device;
    pthread_mutex_lock (&dev_mtx);
    if (*temp == NULL) {
        (*temp) = malloc (sizeof **temp);
        (*temp)->head = *temp;
        (*temp)->next = NULL;
        (*temp)->prev = NULL;
        memset ((*temp)->name, '\0', 16);
        strcpy ((*temp)->name, ip);
        (*temp)->count = 1;
    } else {
        device = malloc (sizeof *device);
        device->head = (*temp)->head;
        device->next = NULL;
        device->prev = (*temp);
        memset (device->name, '\0', 16);
        strcpy (device->name, ip);
        device->count = (*temp)->count + 1;
        (*temp)->count += 1;
        (*temp)->next = device;
        (*temp) = device;
    };
    pthread_mutex_unlock (&dev_mtx);
    return 1;
};

extern int
    dev_unlock (char *ip, _dev_lock * temp) {
    // DELETE ENTRY FROM _dev_lock
    static pthread_mutex_t dev_mtx;

    pthread_mutex_lock (&dev_mtx);

    if (*temp == NULL) {
        write_log (3, "DEBUG: [%s] _dev_lock == NULL!", ip);
        return 0;
    }

    _dev_lock device = (*temp)->head;
    _dev_lock d_prev = NULL,
                       d_next = NULL,
                                d_temp = NULL;

    while (device) {
        if (strcmp (device->name, ip) == 0) {
            if (device->prev && device->next) {
                d_prev = device->prev;
                d_next = device->next;
                d_prev->next = d_next;
                d_next->prev = d_prev;
                d_temp = device;
                device = d_next;
                free (d_temp);
                // write_log(3,"DEBUG: [%s] unlocked by 1", ip);
                pthread_mutex_unlock (&dev_mtx);
                return 1;
            } else if (!device->prev && device->next) {
                d_next = device->next;
                d_next->prev = NULL;
                d_prev = device;
                device = d_next;
                free (d_prev);
                while (d_next) {
                    d_next->head = device;
                    d_next = d_next->next;
                };
                // write_log(3,"DEBUG: [%s] unlocked by 2", ip);
                pthread_mutex_unlock (&dev_mtx);
                return 2;
            } else if (!device->prev && !device->next) {
                free (device);
                device = NULL;
                (*temp) = NULL;
                // write_log(3,"DEBUG: [%s] unlocked by 3", ip);
                pthread_mutex_unlock (&dev_mtx);
                return 3;
            } else if (device->prev && !device->next) {
                d_prev = device->prev;
                d_prev->next = NULL;
                d_temp = device;
                free (d_temp);
                device = NULL;
                (*temp) = d_prev;
                // write_log(3,"DEBUG: [%s] unlocked by 4", ip);
                pthread_mutex_unlock (&dev_mtx);
                return 4;
            }
            // else
            // return 20;
        }
        // write_log(3,"DEBUG: [%s] no luck with %s", ip, device->name);
        device = device->next;
    };
    write_log (3, "DEBUG: [%s] not matched!", ip);
    pthread_mutex_unlock (&dev_mtx);
    return 0;
};
