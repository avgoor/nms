
/**************************************************************************
 *   Copyright (C) 2008 by Denis V. Meltsaykin   *
 *   dvm.avgoor@gmail.com   *
 *                                                                         *
 *   Permission is hereby granted, free of charge, to any person obtaining *
 *   a copy of this software and associated documentation files (the       *
 *   "Software"), to deal in the Software without restriction, including   *
 *   without limitation the rights to use, copy, modify, merge, publish,   *
 *   distribute, sublicense, and/or sell copies of the Software, and to    *
 *   permit persons to whom the Software is furnished to do so, subject to *
 *   the following conditions:                                             *
 *                                                                         *
 *   The above copyright notice and this permission notice shall be        *
 *   included in all copies or substantial portions of the Software.       *
 *                                                                         *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       *
 *   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    *
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. *
 *   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR     *
 *   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, *
 *   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR *
 *   OTHER DEALINGS IN THE SOFTWARE.                                       *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "nms.h"

#define LOG_LEVEL 1
// Maximum number of times_slices, 5 should be enough
#define MAX_TS 5

#define swap(a,b) {int c = (a);(a) = (b);(b) = c;}

struct tm curtime;
time_t now;

pthread_t thread_id[100];
pthread_attr_t thread_attr;
static pthread_mutex_t mtx;
static pthread_mutex_t dev_mtx;
static pthread_mutex_t db_mtx;

static pthread_mutex_t mem_mtx;

pthread_mutex_t pg_mutex;

int LOG;			// fd of logfile
int STAT;			// fd of statfile

_dev_lock main_lock = NULL;

static char authority[] =
    "All code made by Denis V. Meltsaykin (DVM-Avgoor) (C) 2007-2012";

static char version[] = "2.55-12082012";

static char works_type[4][12] = { "None",
                                  "FDB_GET",
                                  "ACL_UPDATE",
                                  "IMPB_UPDATE"
                                };

static char sw_dev[][17] = { "Unknown",
                             "D-Link DES-3526",
                             "D-Link DES-3010G",
                             "D-Link DES-3550",
                             "D-Link DES-3028"
                           };

opt *opts;			// remake

q_un volatile queue;

// static _dev_lock main_lock;

// ==============================================================//
// //
// WRITE TO LOGFILE (level, format, ...) as printf.  //
// "level" should be rather than LOG_LEVEL //
// //
// ==============================================================//

struct tm *
            loctime_wrapper (struct tm *time_x) {
    struct tm *tmout;

    pthread_mutex_lock (&mtx);
    time (&now);
    tmout = localtime (&now);
    if (tmout) {
        memcpy (time_x, tmout, sizeof (struct tm));
        tmout = time_x;
    };
    pthread_mutex_unlock (&mtx);
    return tmout;
};

int
drop_stat (int statfd, q_un * que, qhead * grand, _dev_lock * locks) {

    q_un temp_q;
    // qhead temp_g = (*grand);
    _dev_lock temp_l;
    extern _dev_lock main_lock;

    ftruncate (statfd, 0);
    lseek (statfd, 0, SEEK_SET);

    if (*que) {
        temp_q = (*que)->head;
        dprintf (statfd, "q_un main queue: \n");
        while (temp_q) {
            dprintf (statfd, "[%d,%s,%d,%s]\n", temp_q->count,
                     works_type[temp_q->cmd_type], temp_q->timeslice, temp_q->target);
            temp_q = temp_q->next;
        };
    };

    if (main_lock) {
        temp_l = (main_lock)->head;
        dprintf (statfd, "_dev_lock queue: \n");

        while (temp_l) {
            dprintf (statfd, "[%s]\n", temp_l->name);
            temp_l = temp_l->next;
        };
    };
    return 0;

};

int
log_locked_devices ()
{
    _dev_lock temp_l;
    extern _dev_lock main_lock;

    if (main_lock) {
        temp_l = (main_lock)->head;
        write_log (1, "Locked device list follows:");
        while (temp_l) {
            write_log (1, "Still locked - [%s]", temp_l->name);
            temp_l = temp_l->next;
        };
    };
	return 0;
};

int
write_log (int level, char *str, ...) {
    char out_buf[384];
    va_list args;
    char fileout[512];
    char timestr[100];
    struct tm ct;
    static pthread_mutex_t mtx;

    if (LOG_LEVEL >= level) {
        pthread_mutex_lock (&mtx);
        loctime_wrapper (&ct);
        strftime (timestr, sizeof (timestr), "%F %R:%S", &ct);
        va_start (args, str);
        vsprintf (out_buf, str, args);
        sprintf (fileout, "%s # (%u) %s\n", timestr, (unsigned int) pthread_self() ,out_buf);
        va_end (args);
        write (LOG, fileout, strlen (fileout));
        pthread_mutex_unlock (&mtx);
    };

    return 0;
};

extern void *
    my_malloc (size_t size) {
    static pthread_mutex_t mem_mtx;
    void *p;

    /*
     * Concurrent malloc() is bad
     */
    pthread_mutex_lock (&mem_mtx);
    p = malloc (size);
    pthread_mutex_unlock (&mem_mtx);
    if (p == NULL)
        write_log (1, "malloc(%d) failed!", size);
    memset (p, '\0', size);	/* fill new memory */

    return p;

};

extern void *
    my_realloc (void *old_p, size_t size) {
    static pthread_mutex_t mem_mtx;
    void *p;

    /*
     * So and realloc() too
     */
    pthread_mutex_lock (&mem_mtx);
    p = realloc (old_p, size);
    pthread_mutex_unlock (&mem_mtx);

    if (p == NULL) {
        write_log (1, "realloc (%d) failed!", size);
        return old_p;
    };

    return p;
};

// =====================================================//
// //
// Main cmds queue, and control funtions //
// //
// =====================================================//

typedef struct _time_array {
    short count;
    int timeslice[MAX_TS];
} time_array;

int
queue_renew (q_un * q) {
    int count;
    int a;
    char *tgt;
    char *delim;
    char *type;
    PGresult *resulting;
    int id = 1;
    /*
     * If queue is not empty (i.e. we should really renew it), destroy it
     */
    if ((*q) != NULL) {
        queue_destroy ((q_un *) & (*q));
        (*q) = NULL;
    };

	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    resulting =
        PQexec (res,
                "SELECT DISTINCT sw_ip, delimiter, type FROM switches WHERE aclmode=true;");

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};


    if ((count = PQntuples (resulting)) <= 0) {
        write_log (1, "(!) Can't find any device in database for ACL Update");

    } else {
        write_log (1, "NMS: (Queue debug) Found %d devices for ACL Update", count);
        for (a = 0; a < count; a++) {
            tgt = PQgetvalue (resulting, a, 0);
            delim = PQgetvalue (resulting, a, 1);
            type = PQgetvalue (resulting, a, 2);
            queue_push (q, id++, ACL_UPDATE, PRIO_NORM, opts->update_interval, tgt, delim,
                        type);
        };
    };
    PQclear (resulting);

   	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    resulting =
        PQexec (res,
                "SELECT DISTINCT sw_ip, delimiter, type FROM switches WHERE impbmode=true;");
	
	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};



    if ((count = PQntuples (resulting)) <= 0) {
        write_log (1, "(!) Can't find any device in database for IMPB Update");
    } else {
        write_log (1, "NMS: (Queue debug) Found %d devices for IMPB Update", count);
        for (a = 0; a < count; a++) {
            tgt = PQgetvalue (resulting, a, 0);
            delim = PQgetvalue (resulting, a, 1);
            type = PQgetvalue (resulting, a, 2);
            queue_push (q, id++, IMPB_UPDATE, PRIO_NORM, opts->update_interval, tgt,
                        delim, type);
        };
    };
    PQclear (resulting);

	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    resulting =
        PQexec (res,
                "SELECT DISTINCT sw_ip, delimiter, type FROM switches WHERE bbmode=true;");

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};


    if ((count = PQntuples (resulting)) <= 0) {
        write_log (1, "(!) Can't find any device in database for fdb_get");
        // exit (2);
    } else {
        write_log (1, "NMS: (Queue debug) Found %d devices for fdb_get", count);
        for (a = 0; a < count; a++) {
            tgt = PQgetvalue (resulting, a, 0);
            delim = PQgetvalue (resulting, a, 1);
            type = PQgetvalue (resulting, a, 2);
            queue_push (q, id++, FDB_GET, PRIO_LOW, opts->bb_interval, tgt, delim, type);
        };
    };
    PQclear (resulting);

    return 0;
};

time_array
get_timeslice (q_un q) {
    time_array ts;
    int cc = 0;
    int tt = 0;
    int count = q->count;
    int tsa[count];

    q_un qs = q->head;

    while (qs) {
        tsa[cc] = qs->timeslice;
        cc++;
        qs = qs->next;
    };

    for (cc = 0; cc < count; cc++) {
        for (tt = 0; tt < count; tt++) {
            if (tsa[tt] < tsa[cc])
                swap (tsa[tt], tsa[cc]);
        }
    };
    tt = 0;
    for (cc = 0; cc < count; cc++) {
        if (tsa[cc] != tsa[cc + 1]) {
            tt++;
            if (tt <= MAX_TS) {
                ts.timeslice[tt - 1] = tsa[cc];
                ts.count = tt;
            } else {
                write_log (1, "Too many timeslices, max %d", MAX_TS);
                exit (12);
            };
        };
    };

    return ts;
};

void
s_handler (int signum) {
    if (signum != 15) {
		write_log (1, "ARRIVED %d", signum);
	} else {
	    write_log (1, "RECEIVED signal %d, stopping", signum);
	    close (LOG);
	    close_db ();
	    queue_destroy ((q_un *) & queue);
	    exit (101);
	}
};

/*
 * void alrm_handler (int signum) { write_log (1, "Timeout in operation exceed %d sec.",
 * TELNET_TIMEOUT); // x->lock_state = NOTLOCKED; // signal(SIGALRM, alrm_handler); //
 * pthread_exit ((void *) 1); // pthread_cancel(pthread_self()); // return 1; };
 */

void
work_thread (void *name) {
    fdb_data *x;
    x = (fdb_data *) name;
    write_log (1, "NMS: fdb_get [%s,%s,%d]", x->name, sw_dev[x->type], x->type);
    fdb_get2 (x->name, x->login, x->password, x->delimiter, x->type, x->resurs,
              x->storage_ptr);
    // dev_unlock (x->name, &main_lock);
    // free (x);
};

void
update_thread (void *name) {
    fdb_data *x;
    x = (fdb_data *) name;

    /*
     * We have no acl algorithms to other than DES3526 switches
     */
    /*
     * if ( (x->type != DES3526) && (x->type != DES3550) ) { write_log(1,"NMS: (!) [%s]
     * No ACL Update action defined for %s", x->name, sw_dev[x->type]); };
     */
    write_log (1, "NMS: acl_update [%s,%s,%d, ACL_UPDATE]", x->name, sw_dev[x->type],
               x->type);
    acl_update (x->name, x->login, x->password, x->delimiter, ACL_UPDATE, x->resurs,
                x->storage_ptr, x->type);
    // dev_unlock(x->name, &main_lock);

};

void
impb_thread (void *name) {
    fdb_data *x;
    x = (fdb_data *) name;

    if (x->type == DES3010) {
        write_log (1, "NMS: impb3010_update [%s,%s,%d]", x->name, sw_dev[x->type],
                   x->type);
        impb3010_update (x->name, x->login, x->password, x->delimiter, x->resurs,
                         x->storage_ptr);
    } else {
        acl_update (x->name, x->login, x->password, x->delimiter, IMPB_UPDATE, x->resurs,
                    x->storage_ptr, x->type);
        write_log (1, "NMS: acl_update [%s,%s,%d, IMPB_UPDATE]", x->name, sw_dev[x->type],
                   x->type);
    };
    // dev_unlock(x->name, &main_lock);
};

void
work_switch (q_un entry, threadpool * workers, int work_type) {
    fdb_data *swt_data;

    swt_data = (fdb_data *) my_malloc (sizeof (fdb_data));
    // memset (swt_data, '\0', sizeof (fdb_data));
    memcpy (swt_data->name, entry->target, strlen (entry->target));
    memcpy (swt_data->delimiter, entry->delimiter, strlen (entry->delimiter));
    memcpy (swt_data->login, opts->swlogin, strlen (opts->swlogin));
    memcpy (swt_data->password, opts->swpass, strlen (opts->swpass));
    swt_data->type = entry->type;
    swt_data->resurs = res;
    swt_data->storage_ptr = NULL;

    /*
     * Ready to dive-in :-)
     */

    switch (work_type) {
        case FDB_GET:
            dispatch ((*workers), work_thread, (void *) swt_data);
            break;
        case ACL_UPDATE:
            dispatch ((*workers), update_thread, (void *) swt_data);
            break;
        case IMPB_UPDATE:
            dispatch ((*workers), impb_thread, (void *) swt_data);
            break;
        default:
            break;
    };
    write_log (1, "Dispatched [%s][%d]", swt_data->name, work_type);
};

void
catch_up (q_un tmp) {
    q_un queue;
    void *val = NULL;
    char query[512];
    PGresult *to_base;
    int retc = 0;
    fdb fdb_list;

    // fdb *
    queue = tmp->head;

    while (queue) {
        if ((queue->thread_id != 0)) {

            retc = pthread_join (queue->thread_id, &val);
            write_log (1, "Joined [%d] thread for %s", retc, queue->target);

            if (val != NULL)
                switch (queue->cmd_type) {
                    case FDB_GET:
                        save_fdb ((fdb) val, queue->target, res);
                        fdb_destroy ((fdb *) & val);
                        sprintf (query, "UPDATE switches SET last_connect=(date_trunc"
                                 "( 'second' , 'now'::timestamp )) WHERE sw_ip='%s';",
                                 queue->target);

						if (0 != pthread_mutex_lock (&pg_mutex)) {
							DEBUG ("QUEUE Mutex lock failed!");
							exit (EXIT_FAILURE);
						};
                        
                        to_base = PQexec (res, query);

						if (0 != pthread_mutex_unlock (&pg_mutex)) {
							DEBUG ("QUEUE Mutex unlock failed!");
							exit (EXIT_FAILURE);
						};



                        PQclear (to_base);
                        break;
                    default:
                        break;
                };
            queue->thread_id = 0;
        };
        queue = queue->next;
    };

};

int
main (int argc, char *argv[]) {
    // struct tm *curtime;
    main_lock = NULL;
    time_array new;
    int y;
    int second;
    int counter = 0;
    int iter = 0;
    int been_runned = 0;
    q_un pol;
    pid_t child;
    sigset_t newmask,
    oldmask;
    char logpath[255];
    PGresult *clear_res;

    threadpool workers;

    close (0);
    close (1);
    close (2);

    child = fork ();

    if (child > 0)
        _exit (0);
    setsid ();
    if (fork () > 0)
        _exit (0);
    // chdir("/");
    umask (0);

    // tzset();

    pthread_mutex_init (&mtx, NULL);
    pthread_mutex_init (&dev_mtx, NULL);
    pthread_mutex_init (&db_mtx, NULL);
    pthread_mutex_init (&mem_mtx, NULL);
    pthread_mutex_init (&pg_mutex, NULL);
    
    // setlocale (LC_ALL, "C");

    signal (SIGINT, s_handler);
    signal (SIGTERM, s_handler);
    signal (SIGPIPE, s_handler);
    signal (SIGIO, s_handler);
    signal (SIGURG, s_handler);
    
    sigfillset (&newmask);
    
    sigdelset (&newmask, SIGTERM);
    sigdelset (&newmask, SIGINT);
	sigdelset (&newmask, SIGIO);
	sigdelset (&newmask, SIGURG);
	sigdelset (&newmask, SIGPIPE);
	
    if (pthread_sigmask (SIG_BLOCK, &newmask, &oldmask) < 0)
        write_log (1, "NMS: (DEBUG) Fail to set sigblock");

    memset ((void *) &main_lock, '\0', sizeof (main_lock));
    // sprintf (logpath, "%s/nms.log", opts->log_dir);

    if ((LOG =
                open ("/var/log/nms_test/nms.log", O_CREAT | O_WRONLY | O_NONBLOCK | O_APPEND,
                      (mode_t) 0666)) < 0)
        perror ("Logfile open failed");

    dup2 (LOG, 1);
    dup2 (LOG, 2);

    if ((STAT =
                open ("/var/log/nms_test/nms.stat", O_CREAT | O_WRONLY | O_NONBLOCK,
                      (mode_t) 0666)) < 0)
        perror ("Statfile open failed");

    opts = (opt *) malloc (sizeof *opts);

    write_log (1, "%s",
               "-------------------------------------------------------------------------");
    write_log (1, "%s %d", "\t\tStarted main pid:", getpid ());

	write_log (1, "Main controller version: %s", version);

    parse_config ("./nms.conf", opts);

    if (open_db (opts) < 0) {
        write_log (1, "(!) Can't work without database!");
        close (LOG);
        exit (11);
    }

    if ((!opts->swlogin) || (!opts->swpass)) {
        write_log (1, "(!) no options supplied");
        return 10;
    };

    queue = (q_un) NULL;
    queue_renew ((q_un *) & queue);

    new = get_timeslice (queue);

    memset (&thread_id, 0, sizeof (thread_id));

    y = 0;
    signal (SIGINT, s_handler);

    /*
     *
     *      MAIN LOOP
     *
     */

    workers = create_threadpool (opts->threads_count);

    for (;;) {
        loctime_wrapper (&curtime);	/* Mutexed wrapper, for crappy localtime */

        /*
         * Run trough all registered timeslices
         */

        been_runned = 0;
        for (counter = 0; counter < new.count; counter++) {
            pol = queue->head;
            loctime_wrapper (&curtime);
            /*
             * If current minute of hour equals one of registered timeslices, go further
             */
            if ((curtime.tm_min % new.timeslice[counter]) == 0) {

                /*
                 * Run through queue
                 */
                for (; pol; pol = pol->next)

                    /*
                     * Find the appropriate timeslice in queue, for run the job
                     */
                    if (pol->timeslice == new.timeslice[counter]) {
                        if (!dev_islocked (pol->target, &main_lock)) {
                            iter++;
                            // dev_lockup(pol->target, &main_lock);
                            /*
                             * Jump into pre-dispatch function
                             */
                            work_switch (pol, &workers, pol->cmd_type);
                        } else
                            write_log (3, "MAIN: FDB_GET device %s is locked,"
                                       " waiting for release", pol->target);
                    };
                /*
                 * else write_log (3, "MAIN: pol->timeslice(%d)!= new.timeslice(%d)(cntr
                 * %d)", pol->timeslice, new.timeslice[counter], counter);
                 */
                /*
                 * Sleep till run out from current minute
                 */
                been_runned = 1;
                loctime_wrapper (&curtime);
                write_log (1, "Going sleep for %d seconds (%d)", (65 - curtime.tm_sec),
                           curtime.tm_sec);
                drop_stat (STAT, (q_un *) & queue, NULL, &main_lock);
                sleep (65 - curtime.tm_sec);

				if (0 != pthread_mutex_lock (&pg_mutex)) {
					DEBUG ("QUEUE Mutex lock failed!");
					exit (EXIT_FAILURE);
				};
				
                clear_res = PQexec (res, "VACUUM ANALYZE fdb;");

				if (0 != pthread_mutex_unlock (&pg_mutex)) {
					DEBUG ("QUEUE Mutex unlock failed!");
					exit (EXIT_FAILURE);
				};

                PQclear (clear_res);
                write_log (1, "NMS: main() VACUUM ANALYZE sent to postgres");
            }
        };
        /*
         * Sleep till the end of minute
         */
        if (been_runned == 0) {
            loctime_wrapper (&curtime);
            if (curtime.tm_sec < 30) {
            	if (main_lock != NULL) {log_locked_devices();}
	             else switch (curtime.tm_min) {
                    case 14:
                    case 27:
                    case 43:
                    case 59:
                        queue_renew ((q_un *) & queue);	/* renew queue at that minutes of
							 * hour */
                        break;
                    default:
                        break;
                };
			};
            if ((second = (59 - curtime.tm_sec)) <= 0)
                second = 9;
            write_log (1, "Going sleep for %d seconds", second);
            sleep (second);
        };

    };
    // pthread_join(thread_id,NULL);
    write_log (1, "Exit loop");

    write_log (1, "No active tasks. Quitting.");
    close_db ();
    queue_destroy ((q_un *) & queue);
    close (LOG);
    opts_free (opts);

    return EXIT_SUCCESS;
}
