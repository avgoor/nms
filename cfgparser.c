
/***************************************************************************
 *   Copyright (C) 2008 by Denis V. Meltsaykin   			   *
 *   dvm.avgoor@gmail.com   						   *
 *                                                                         *
 *   Permission is hereby granted, free of charge, to any person obtaining *
 *   a copy of this software and associated documentation files (the       *
 *   "Software"), to deal in the Software without restriction, including   *
 *   without limitation the rights to use, copy, modify, merge, publish,   *
 *   distribute, sublicense, and/or sell copies of the Software, and to    *
 *   permit persons to whom the Software is furnished to do so, subject to *
 *   the following conditions:                                             *
 *                                                                         *
 *   The above copyright notice and this permission notice shall be        *
 *   included in all copies or substantial portions of the Software.       *
 *                                                                         *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       *
 *   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    *
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*
 *   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR     *
 *   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, *
 *   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR *
 *   OTHER DEALINGS IN THE SOFTWARE.                                       *
 ***************************************************************************/

#include "nms.h"

#define BUFS 1024
#define MAX_OPCODE 64
#define MAX_VALUE 128

// ==============================================================================//
// //
// Main functional for parsing conf-files.  //
// opcodes - enum list for link with keys[] //
// keys - struct that links symbolical name with opcodes //
// //
// ==============================================================================//

typedef enum {
    oUnknown = 0, oPass, oLogin, oDBhost, oDBpass, oDBlogin,
    oSW_delimiter, oDBname, oBBinterval, oUpdateinterval, oMallocSize,
    oThrCount, oLogDir, oSwLogDir
} opcodes;

// static opt * option;

static struct {
    const char *name;
    opcodes opcode;
} keys[] = {
    {
        "pass", oPass}, {
        "login", oLogin}, {
        "dbhost", oDBhost}, {
        "dbpass", oDBpass}, {
        "dblogin", oDBlogin}, {
        "sw_delimiter", oSW_delimiter}, {
        "dbname", oDBname}, {
        "bb_interval", oBBinterval}, {
        "update_interval", oUpdateinterval}, {
        "malloc_size", oMallocSize}, {
        "threads_count", oThrCount}, {
        "log_dir", oLogDir}, {
        "sw_log_dir", oSwLogDir}, {
        NULL, oUnknown}
};

// ======================================================================//
// //
// PARSE TOKENS (i.e. parse string "pass" into opcode oPass) //
// //
// ======================================================================//

static opcodes
parse_token (char *common) {
    int i;
    for (i = 0; keys[i].name; i++)
        if ((memcmp (common, keys[i].name, strlen (common))) == 0)
            return keys[i].opcode;

    return oUnknown;

};

int
make_options (opcodes opc, char *val, opt * option) {
    int len = 0;

    switch (opc) {
        case oPass:
            len = strlen (val);
            option->swpass = malloc (len + 2);
            option->swpass = memcpy (option->swpass, val, len);
            option->swpass[len] = '\n';
            option->swpass[len + 1] = '\0';
            break;
        case oLogin:
            len = strlen (val);
            option->swlogin = malloc (len + 2);
            // option->swlogin[0] = '\0';
            option->swlogin = memcpy (option->swlogin, val, len);
            option->swlogin[len] = '\n';
            option->swlogin[len + 1] = '\0';
            break;
        case oDBhost:
            option->dbhost = strdup (val);
            break;
        case oDBpass:
            option->dbpass = strdup (val);
            break;
        case oDBlogin:
            option->dblogin = strdup (val);
            break;
        case oLogDir:
            option->log_dir = strdup (val);
            break;
        case oSwLogDir:
            option->sw_log_dir = strdup (val);
            break;
        case oSW_delimiter:
            option->sw_delimiter = strdup (val);
            break;
        case oDBname:
            option->dbname = strdup (val);
            break;
        case oBBinterval:
            option->bb_interval = atoi (val);
            break;
        case oUpdateinterval:
            option->update_interval = atoi (val);
            break;
        case oMallocSize:
            option->malloc_size = atoi (val);
            break;
        case oThrCount:
            option->threads_count = atoi (val);
            break;
        default:
            return 1;
            break;
    };
    return 0;
};

// ======================================================================//
// //
// PARSE STRING (i.e. parse string "pass=123qwe" into "pass" and //
// "123qwe" then throw "pass" to parse_token) //
// //
// ======================================================================//

int
parse_string (char *subject, opt * option) {
    int status = -1;
    opcodes pt;

    if (strlen (subject) < 2)
        return (11);

    char *token = malloc (MAX_OPCODE + 1);
    //
    // Interesting moment. Fill memory at (char *) token with nulls and
    // copy pointer to pretoken.
    //
    char *pretoken = memset (token, 0, MAX_OPCODE);
    char *value = malloc (MAX_VALUE + 1);
    char *prevalue = memset (value, 0, MAX_VALUE);

    for (; (*subject) != '='; (void) *subject++)
        if (*subject != ' ')
            *pretoken++ = *subject;

    for (; (*subject); (void) *subject++)
        if ((*subject != ' ') && (*subject != '='))
            *prevalue++ = *subject;

    if ((pt = parse_token (token)) != oUnknown) {
        make_options (pt, value, option);
        status = 1;
    } else
        status = 0;

    free (token);
    free (value);
    return status;
};

// ======================================================================//
// //
// PARSE FILE (i.e. parse string "pass" into opcode oPass) //
// //
// ======================================================================//

extern int
    parse_config (char *cfgfile, opt * option) {
    int fd;
    // char symbol;
    int count;
    char *store = malloc (250);
    char *parsestr = malloc (sizeof (char *));
    char *sparse;
    char *pBUFF = malloc (BUFS);
    // char password;
    int c = 0;
    write_log (2, "CFGPARSER: Start config parser");

    if ((fd = open (cfgfile, O_RDONLY, 0644)) < 0) {
        perror ("open");
        write_log (1, "(!) Couldn't open config file:%s", cfgfile);
        return 11;
    };

    memset (parsestr, 0, sizeof (char *));
    memset (pBUFF, 0, BUFS);
    memset (store, 0, 250);

    while ((count = read (fd, pBUFF, BUFS)) > 0) {
        parsestr = realloc (parsestr, strlen (parsestr) + strlen (pBUFF) + 3);
        strncat (parsestr, pBUFF, count);
    };

    close (fd);
    free (pBUFF);

    sparse = parsestr;		// save pointer to parsestr for free()

    while (*parsestr) {
        switch (*parsestr) {
                /*
                 * Skip newlines & whitespaces
                 */

            case '\n':
            case ' ':
                break;

                /*
                 * Strip comments
                 */

            case '#':
            case '/':
                while (*parsestr && *parsestr != '\n') {
                    (void) *parsestr++;
                };
                break;

                /*
                 * Make things with real data :)
                 */

            default: {
                c = 0;
                memset (store, 0, 250);
                while (*parsestr && *parsestr != '\n') {
                    store[c] = *parsestr;
                    c++;
                    (void) *parsestr++;
                };
                store[c++] = '\0';
                if (!parse_string (store, option)) {
                    write_log (1, "(!) CFGPARSER: parse_config(): Unknown token: %s",
                               store);
                } else {
                    write_log (2, "Matched token: %s", store);
                };
            };
            break;
        }
        (void) *parsestr++;
    };

    free (sparse);
    free (store);
    write_log (2, "CFGPARSER: End config parser");
    return 0;
}
