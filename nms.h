
/***************************************************************************
 *   Copyright (C) 2008 by Denis V. Meltsaykin				   *
 *   dvm.avgoor@gmail.com						   *
 *                                                                         *
 *   Permission is hereby granted, free of charge, to any person obtaining *
 *   a copy of this software and associated documentation files (the       *
 *   "Software"), to deal in the Software without restriction, including   *
 *   without limitation the rights to use, copy, modify, merge, publish,   *
 *   distribute, sublicense, and/or sell copies of the Software, and to    *
 *   permit persons to whom the Software is furnished to do so, subject to *
 *   the following conditions:                                             *
 *                                                                         *
 *   The above copyright notice and this permission notice shall be        *
 *   included in all copies or substantial portions of the Software.       *
 *                                                                         *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       *
 *   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    *
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*
 *   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR     *
 *   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, *
 *   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR *
 *   OTHER DEALINGS IN THE SOFTWARE.                                       *
 ***************************************************************************/
// #define _GNU_SOURCE
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <locale.h>

#include <pcre.h>

#include <time.h>

#include <error.h>
#include <errno.h>

#include <signal.h>

#include <assert.h>

#include <setjmp.h>

#include <ctype.h>

#include "libpq-fe.h"

// #define BUFF 1024

// 
// Priorities 
// 

#define PRIO_HIGH	1
#define PRIO_NORM	2
#define PRIO_LOW	3

// 
// JOB TYPES
// 

#define FDB_GET		1
#define	ACL_UPDATE	2
#define	IMPB_UPDATE	3

#define DES3526		1
#define DES3010		2
#define DES3550		3
#define DES3028		4

// 
// ACCESS/DENY RULE DEFINITIONS
// 

#define PERMIT		1
#define	DENY		0

// 
// MAXIMUM DEVICES IN QUEUE (USED IN _dev_lock)
// deprecated after changing _dev_lock to linked-list

#define MAX_DEVS	1024

// 
// 
// 

#define ISLOCKED 	1
#define	NOTLOCKED	0

#define TELNET_TIMEOUT	60

// 
// DECLARATIONS FROM _GNU_SOURCE 
// CAN'T USE _GNU_SOURCE DIRECTLY, BREAKS strerror_r()
// 
void *memmem (const void *, size_t, const void *, size_t);
int dprintf (int, const char *, ...);


#define DEBUG(n) write_log(1, "%s:%d %s", __FILE__, __LINE__, n);

extern PGconn *res;
// extern PGresult *result;

extern pthread_mutex_t pg_mutex;

typedef void *threadpool;
typedef void (*dispatch_fn) (void *);

threadpool create_threadpool (int num_threads_in_pool);
void dispatch (threadpool from_me, dispatch_fn dispatch_to_here, void *arg);
void dispatch_with_cleanup (threadpool from_me, dispatch_fn dispatch_to_here,
			    void *arg, dispatch_fn cleaner_func, void *cleaner_arg);
void destroy_threadpool (threadpool destroyme);
void destroy_threadpool_immediately (threadpool destroymenow);

typedef struct qnode {
    dispatch_fn func_to_dispatch;
    void *func_arg;
    dispatch_fn cleanup_func;
    void *cleanup_arg;
    struct qnode *next;
    struct qnode *prev;
} qnode;

typedef struct qhead {
    struct qnode *head;		// worker nodes
    struct qnode *tail;
    struct qnode *fhead;	// free nodes, i.e. not in work
    struct qnode *ftail;
    int capacity;
    int max_capacity;
} qhead;

typedef enum {
    ALL_RUN, ALL_EXIT
} poolstate_t;

typedef struct _threadpool_st {
    struct timeval created;	// When the threadpool was created.
    pthread_t *array;		// The threads themselves.

    pthread_mutex_t mutex;	// protects all vars declared below.
    pthread_mutex_t queue_mutex;
    pthread_cond_t job_posted;	// dispatcher: "Hey guys, there's a job!"
    pthread_cond_t job_taken;	// a worker: "Got it!"

    poolstate_t state;		// Threads check this before getting job.
    int arrsz;			// Number of entries in array.
    int live;			// Number of live threads in pool (when
    // pool is being destroyed, live<=arrsz)

    qhead *job_queue;		// queue of work orders
} _threadpool;

typedef struct _opt {
    char *swpass;
    char *swlogin;
    char *dbhost;
    char *dbpass;
    char *dblogin;
    char *sw_delimiter;
    char *dbname;
    char *log_dir;
    char *sw_log_dir;
    int update_interval;
    int bb_interval;
    int malloc_size;
    int threads_count;
} opt;

typedef struct _fdb {
    volatile int vlan;
    char *mac;
    int port;
    struct _fdb *head;
    struct _fdb *next;
}   *fdb;

typedef struct _rdb {
    // char string[384];
    char *string;
    int port;
    int access_id;
    int profile_id;
    int action;
    struct _rdb *head;
    struct _rdb *next;
    struct _rdb *prev;
}   *rdb;

typedef struct _pdb {
    // char profile[384];
    char *profile;
    struct _pdb *head;
    struct _pdb *next;
    struct _pdb *prev;
}   *pdb;

typedef struct _q_un_ {
    int count;
    int cmd_id;
    short cmd_type;
    short cmd_priority;
    short timeslice;
    short lock_state;
    char *target;
    char *delimiter;
    int type;
    pthread_t thread_id;
    struct _q_un_ *head;
    struct _q_un_ *next;
    struct _q_un_ *prev;
}     *q_un;

typedef struct _dv_lock {
    int count;
    char name[16];
    struct _dv_lock *head;
    struct _dv_lock *next;
    struct _dv_lock *prev;
}       *_dev_lock;

typedef struct {
    char name[16];
    char delimiter[32];
    char login[128];
    char password[128];
    int type;
    PGconn *resurs;
    char *storage_ptr;
} fdb_data;

// 
// LOCK ROUTINES. REFERENCED IN structs.c
// 

extern int dev_islocked (char *, _dev_lock *);
extern int dev_lockup (char *, _dev_lock *);
extern int dev_unlock (char *, _dev_lock *);

// extern _dev_lock main_lock;

// extern _opt option;

fdb fdb_parse (char *, int);

extern void *my_malloc (size_t);

extern void *my_realloc (void *, size_t);

extern int queue_push (q_un * que, int cmd_id, short cmd_type,
		       short cmd_priority, short timeslice,
		       char *target, char *delimiter, char *);

extern int queue_destroy (q_un * list);

extern int fdb_get (char *, char *, char *, char *);

extern int fdb_get2 (char *, char *, char *, char *, int, PGconn *, char *);

extern int fdb_insert (fdb *, char *, int, int);

extern int save_fdb (fdb, char *, PGconn *);

extern int fdb_destroy (fdb *);

extern int rdb_insert (rdb *, char *, int, int, int, int);

extern int rdb_destroy (rdb *);

extern int rdb_delete (rdb *);

extern int pdb_insert (pdb *, char *);

extern int pdb_destroy (pdb *);

extern int pdb_delete (pdb *);

extern int acl_update (char *, char *, char *, char *, int, PGconn *, char *, int);

extern int impb3526_update (char *, char *, char *, char *);

extern int impb3010_update (char *, char *, char *, char *, PGconn *, char *);

extern pdb parse_3010_cfg (char *, pdb *);

extern int write_log (int, char *, ...);

extern int opts_free (opt * a);

extern int parse_config (char *, opt *);

extern int open_db (opt *);

extern int close_db ();

extern rdb make_rules_db (char *, pdb *, PGconn *, int, int);

extern pdb create_bindings (char *, pdb *, PGconn *);

extern rdb make_impb_db (char *, pdb *);

extern rdb rdb_compare (rdb *, rdb *, pdb *, int);

extern pdb pdb_compare (pdb *, pdb *);

extern pdb impb_compare (pdb *, pdb *, pdb *, pdb *, int);

extern char *prof_parse (char *);

extern char *impb_prof_delete (char *);

extern char *impb_rule_delete (char *);

extern char *prof_delete (char *);

extern char *rule_delete (int, int);

extern char *rdb2rule (rdb *);

// redefine
// int pcre_get_substring(const char *, int *, int, int , char **);
