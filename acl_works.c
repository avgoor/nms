
/***************************************************************************
 *   Copyright (C) 2008 by Denis V. Meltsaykin   			   *
 *   dvm.avgoor@gmail.com   						   *
 *                                                                         *
 *   Permission is hereby granted, free of charge, to any person obtaining *
 *   a copy of this software and associated documentation files (the       *
 *   "Software"), to deal in the Software without restriction, including   *
 *   without limitation the rights to use, copy, modify, merge, publish,   *
 *   distribute, sublicense, and/or sell copies of the Software, and to    *
 *   permit persons to whom the Software is furnished to do so, subject to *
 *   the following conditions:                                             *
 *                                                                         *
 *   The above copyright notice and this permission notice shall be        *
 *   included in all copies or substantial portions of the Software.       *
 *                                                                         *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       *
 *   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    *
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*
 *   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR     *
 *   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, *
 *   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR *
 *   OTHER DEALINGS IN THE SOFTWARE.                                       *
 ***************************************************************************/

#include "nms.h"

/*
 * // select mac,host from users left join user_addr on
 * (users.host=user_addr.login) // where users.money>0 and frozen=0 and
 * user_addr.addr ilike 'Копылова 36' or user_addr.addr ilike
 * 'Копылова 36-%'; // //or
 *
 * change users.money to float on lsdb!!
 *
 * SELECT distinct bindings.login, users.mac, users.money,
 * bindings.street, bindings.building, bindings.flat, sw_ports.sw_port
 * from bindings,users,sw_ports where (bindings.login=users.login) and
 * bindings.street=sw_ports.port_entry and
 * bindings.building=sw_ports.port_entry_next and users.money>0 and
 * sw_ports.is_building and sw_ports.sw_ip='10.5.0.21' order by
 * sw_ports.sw_port asc;
 *
 */

inline char *
mac_make (register char *mac) {
    char *ptr = mac;
    // *ptr = toupper (*ptr);
    while (*ptr) {
        if (*ptr == ':')
            *ptr = '-';
        else
            *ptr = toupper (*ptr);
        (void) *ptr++;
    };
    return mac;

};

int
compare_strings (register char *first, register char *second) {
    register char *f_ptr,
    *s_ptr;

    int char_count = 0;
    int cmp = 1;

    f_ptr = first;
    s_ptr = second;

    if (strlen (f_ptr) > strlen (s_ptr))
        char_count = strlen (s_ptr);
    else
        char_count = strlen (f_ptr);

    while ((--char_count > 0) && (*s_ptr) && (*f_ptr) && ((*s_ptr) == (*f_ptr))) {
        *s_ptr++;
        *f_ptr++;
        cmp++;
    };

    if (char_count == 0)
        return 0;
    else
        return cmp;
};

pdb
create_bindings (char *sw_ip, pdb * profile, PGconn * resurs) {
    extern opt *opts;
    // PGconn *resurs;
    PGresult *result;
    char query[250];
    int count = 0;
    char conn_data[250];
    char bigstr[1024];

    pdb bindings = NULL;

    pdb prof = NULL;
    /*
     * sprintf (conn_data, "dbname=%s", (*opts).dbname); resurs = PQconnectdb
     * (conn_data);
     *
     * if (PQstatus (resurs) == CONNECTION_OK) write_log (1, "ACL_WORKS: [%s]
     * make_bindings() : connection to database established", sw_ip); else { write_log
     * (1, "ACL_WORKS: (!) make_bindings: connection to database failed: %s",
     * PQerrorMessage (resurs)); PQfinish (resurs); return NULL; };
     */
    /*
     * config address_binding ip_mac ports 5 state enable allow_zeroip enable
     */
    /*
     * SELECT distinct sw_port from sw_ports where sw_ip='10.1.1.6';
     */

    memset (bigstr, '\0', sizeof bigstr);
    memset (query, '\0', sizeof query);

    sprintf (query, "SELECT distinct sw_port from sw_ports where sw_ip='%s';", sw_ip);

	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

    for (count = 0; count < PQntuples (result); count++) {
        sprintf (bigstr,
                 "config address_binding ip_mac ports %s state enable allow_zeroip enable\n",
                 PQgetvalue (result, count, 0));
        pdb_insert (&prof, bigstr);
    };
    PQclear (result);

    memset (bigstr, '\0', sizeof bigstr);
    memset (query, '\0', sizeof query);
    /*
     *  SELECT users.ip, users.mac, sw_ports.sw_port from users, sw_ports where users.login=sw_ports.port_entry
     *  and sw_ports.sw_ip='10.1.1.6' and sw_ports.is_client;
     */
    sprintf (query,
             "SELECT users.ip, users.mac, sw_ports.sw_port from users, sw_ports where "
             "users.login=sw_ports.port_entry and sw_ports.sw_ip='%s' and sw_ports.is_client ORDER BY sw_port;",
             sw_ip);

   	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

    
    for (count = 0; count < PQntuples (result); count++) {
        /*
         * create address_binding ip_mac ipaddr 172.11.0.11 mac_address AA-BB-CC-11-AA-EE
         * ports 2
         */
        sprintf (bigstr,
                 "create address_binding ip_mac ipaddr %s mac_address %s ports %s\n",
                 PQgetvalue (result, count, 0), mac_make (PQgetvalue (result, count, 1)),
                 PQgetvalue (result, count, 2));
        pdb_insert (&bindings, bigstr);
    };
    PQclear (result);
    // PQfinish (resurs);
    (*profile) = prof;
    return bindings;
}

int
create_access_users (PGconn * resurs, char *sw_ip, char *profile_text,
                     char *rule_text, rdb * list) {

    PGresult *result;
    char query[1024];
    int count;
    char bigstr[1024];

    write_log (3, "[%s] ACL_WORKS: create_access_users() - started", sw_ip);
    /*
     * query = my_malloc (1024); bigstr = my_malloc (1024);
     */

    memset (query, '\0', 1024);

    sprintf (query,
             "SELECT distinct bindings.login, users.mac, sw_ports.sw_port, "
             "bindings.street, bindings.building, bindings.flat, users.money "
             "from bindings,users,sw_ports where "
             "(bindings.login=users.login) and bindings.street=sw_ports.port_entry "
             "and bindings.building=sw_ports.port_entry_next "
             "and sw_ports.is_building and sw_ports.sw_ip='%s' and users.mac<>'' "
             "and users.frozen=0 and users.ip<>'' and users.money>=0 and users.mac<>'' and users.blocked=0 "
             "order by sw_ports.sw_port asc;\n",
             sw_ip);

   	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};


    for (count = 0; count < PQntuples (result); count++) {
        // config access_profile profile_id 50 add access_id %d ethernet
        // source_mac %s desti
        memset (bigstr, '\0', 1024);
        sprintf (bigstr,
                 "ethernet source_mac %s destination_mac 00-11-22-33-44-55",
                 mac_make (PQgetvalue (result, count, 1)));
        rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 2)), 0, 50, PERMIT);
    }
    PQclear (result);

    memset (query, '\0', 1024);

    sprintf (query,
             "SELECT distinct users.login, users.mac, sw_ports.sw_port from users,"
             " sw_ports where sw_ports.sw_ip='%s' and is_client and sw_ports.port_entry ilike users.login "
             " and users.frozen=0 and users.ip<>'' and users.mac<>'' and users.money>=0;\n",
             sw_ip);
             
	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};
             
    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};


    for (count = 0; count < PQntuples (result); count++) {
        // config access_profile profile_id 50 add access_id %d ethernet
        // source_mac %s desti
        memset (bigstr, '\0', 1024);
        sprintf (bigstr,
                 "ethernet source_mac %s destination_mac 00-11-22-33-44-55",
                 mac_make (PQgetvalue (result, count, 1)));
        rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 2)), 0, 50, PERMIT);
    }

    PQclear (result);
    // free (query);
    // free (bigstr);
    return 0;
};

int
create_ip_access_users (PGconn * resurs, char *sw_ip, char *profile_text,
                        char *rule_text, rdb * list) {

    PGresult *result;
    char query[1024];
    int count;
    char bigstr[1024];

    write_log (3, "[%s] ACL_WORKS: create_ip_access_users() - started", sw_ip);
    /*
     * query = my_malloc (1024); bigstr = my_malloc (1024);
     */

    memset (query, 0, 1024);

    sprintf (query,
             "SELECT DISTINCT ip, port from users, switches  where switches.building=users.building "
             "and switches.street=users.street and switches.accesspoint=users.accesspoint "
             "and sw_ip='%s' and users.ip<>'' and users.mac<>'' and users.money>=0 and frozen=0 and blocked=0 order by port;",
             sw_ip);

	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

    for (count = 0; count < PQntuples (result); count++) {
        // config access_profile profile_id 50 add access_id %d ethernet
        // source_mac %s desti
        memset (bigstr, '\0', 1024);
        sprintf (bigstr, "ip source_ip %s", PQgetvalue (result, count, 0));
        rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 1)), 0, 50, PERMIT);
    }

    PQclear (result);

    return 0;
};

int
create_arp_rule (PGconn * resurs, char *sw_ip, char *profile_text, char *rule_text,
                 rdb * list) {

    PGresult *result;
    char query[1024];
    int count;
    char bigstr[1024];
    int deleted = 0;

    write_log (3, "[%s] ACL_WORKS: create_arp_rule() - started", sw_ip);
    /*
     * query = my_malloc (1024); bigstr = my_malloc (1024);
     */
    memset (query, '\0', 1024);

    sprintf (query,
             "SELECT DISTINCT mac, ip, port from users, switches  where switches.building=users.building "
             "and switches.street=users.street and switches.accesspoint=users.accesspoint "
             "and sw_ip='%s' and users.ip<>'' and users.mac<>'' and blocked=0 order by port;", sw_ip);
	
	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

    for (count = 0; count < PQntuples (result); count++) {
        char *mac = NULL,
                    *ip = NULL;
        char ipx[4][4] = {
            "\0\0\0",
            "\0\0\0",
            "\0\0\0",
            "\0\0\0"
        };
        int ipz[4] = { 0 };
        int i = 0,
                z = 0,
                    x = 0;
        char macx[2][10] = { };

        mac = PQgetvalue (result, count, 0);
        ip = PQgetvalue (result, count, 1);
        int iplen = strlen (ip);

        deleted = 0;

        for (i = 0; i < 5; ++i) {
            if (deleted == 0) {
                if (mac[i] != '0' && mac[i] != ':') {
                    deleted = 1;
                    macx[0][x] = toupper (mac[i]);
                    x++;
                }
            } else if (mac[i] != ':') {
                macx[0][x] = toupper (mac[i]);
                x++;
            }
        };

        macx[0][x] = '\0';
        x = 0;
        deleted = 0;

        for (i = 6; i < 17; ++i) {
            if (deleted == 0) {
                if (mac[i] != '0' && mac[i] != ':') {
                    deleted = 1;
                    macx[1][x] = toupper (mac[i]);
                    x++;
                }
            } else if (mac[i] != ':') {
                macx[1][x] = toupper (mac[i]);
                x++;
            }
        };

        macx[1][x] = '\0';

        x = 0;
        for (i = 0; i < iplen; i++) {
            if (ip[i] != '.')
                ipx[z][x++] = ip[i];
            else {
                z++;
                x = 0;
            }
        }

        ipz[0] = atoi (ipx[0]);
        ipz[1] = atoi (ipx[1]);
        ipz[2] = atoi (ipx[2]);
        ipz[3] = atoi (ipx[3]);
        memset (bigstr, '\0', 1024);
        sprintf (bigstr, "packet_content_mask offset_0-15  0x0  0x%s "
                 " 0x%s  0x0 offset_16-31  0x8060000  0x0  0x%s  0x%s offset_32-47 "
                 " 0x%02X%02X%02X%02X  0x0  0x0  0x0", macx[0], macx[1], macx[0], macx[1]
                 , ipz[0], ipz[1], ipz[2], ipz[3]);

        /*
         * sprintf (bigstr, "ethernet source_mac %s destination_mac 00-11-22-33-44-55",
         * mac_make (PQgetvalue (result, count, 1)));
         */
        rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 2)), 0, 45, PERMIT);
    }

    PQclear (result);
    /*
     * free (query); free (bigstr);
     */
    return 0;
};

int
create_ip_rule (PGconn * resurs, char *sw_ip, char *profile_text, char *rule_text,
                rdb * list) {

    PGresult *result;
    char query[1024];
    int count;
    char bigstr[1024];
    int deleted = 0;

    write_log (3, "[%s] ACL_WORKS: create_ip_rule() - started", sw_ip);
    /*
     * query = my_malloc (1024); bigstr = my_malloc (1024);
     */
    memset (query, '\0', 1024);

    sprintf (query,
             "SELECT DISTINCT mac, ip, port from users, switches  where switches.building=users.building "
             "and switches.street=users.street and switches.accesspoint=users.accesspoint "
             "and sw_ip='%s' and users.ip<>'' and users.mac<>'' and users.money>=0 and frozen=0 and blocked=0 order by port;",
             sw_ip);
   	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};


    for (count = 0; count < PQntuples (result); count++) {
        char *mac = NULL,
                    *ip = NULL;
        char ipx[4][4] = {
            "\0\0\0",
            "\0\0\0",
            "\0\0\0",
            "\0\0\0"
        };
        int ipz[4] = { 0 };
        int i = 0,
                z = 0,
                    x = 0;
        char macx[2][10];

        mac = PQgetvalue (result, count, 0);
        ip = PQgetvalue (result, count, 1);
        int iplen = strlen (ip);
        deleted = 0;

        for (i = 0; i < 5; ++i) {
            if (deleted == 0) {
                if (mac[i] != '0' && mac[i] != ':') {
                    deleted = 1;
                    macx[0][x] = toupper (mac[i]);
                    x++;
                }
            } else if (mac[i] != ':') {
                macx[0][x] = toupper (mac[i]);
                x++;
            }
        };

        macx[0][x] = '\0';
        x = 0;
        deleted = 0;

        for (i = 6; i < 17; ++i) {
            if (deleted == 0) {
                if (mac[i] != '0' && mac[i] != ':') {
                    deleted = 1;
                    macx[1][x] = toupper (mac[i]);
                    x++;
                }
            } else if (mac[i] != ':') {
                macx[1][x] = toupper (mac[i]);
                x++;
            }
        };

        macx[1][x] = '\0';

        x = 0;
        for (i = 0; i < iplen; i++) {
            if (ip[i] != '.')
                ipx[z][x++] = ip[i];
            else {
                z++;
                x = 0;
            }
        }

        ipz[0] = atoi (ipx[0]);
        ipz[1] = atoi (ipx[1]);
        ipz[2] = atoi (ipx[2]);
        ipz[3] = atoi (ipx[3]);
        memset (bigstr, '\0', 1024);
        sprintf (bigstr,
                 "packet_content_mask offset_0-15  0x0  0x%s  0x%s  0x0 offset_16-31 "
                 " 0x8000000  0x0  0x0  0x%02X%02X offset_32-47  0x%02X%02X0000  0x0  0x0  0x0",
                 macx[0], macx[1], ipz[0], ipz[1], ipz[2], ipz[3]);

        /*
         * sprintf (bigstr, "ethernet source_mac %s destination_mac 00-11-22-33-44-55",
         * mac_make (PQgetvalue (result, count, 1)));
         */
        rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 2)), 0, 55, PERMIT);
    }

    PQclear (result);
    /*
     * free (query); free (bigstr);
     */
    return 0;
};

int
create_eth_deny (PGconn * resurs, char *sw_ip, char *profile_text,
                 char *rule_text, rdb * list) {

    PGresult *result;
    char query[1024];
    int count;
    char bigstr[1024];
    write_log (3, "[%s] ACL_WORKS: create_deny() - started", sw_ip);
    /*
     * query = my_malloc (1024); bigstr = my_malloc (1024);
     */
    memset (query, 0, 1024);
    sprintf (query,
             "SELECT distinct port from users, switches  where switches.building=users.building "
             "and switches.street=users.street and switches.accesspoint=users.accesspoint "
             "and sw_ip='%s' order by port;\n", sw_ip);

	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};


    for (count = 0; count < PQntuples (result); count++) {
        // config access_profile profile_id 30 add access_id %d ip tcp
        // dst_port %d port %d %s
        memset (bigstr, '\0', 1024);
        sprintf (bigstr, "ethernet source_mac 11-22-33-44-55-66");
        rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 0)), 0, 60, DENY);

    }
    PQclear (result);
    /*
     * free (query); free (bigstr);
     */
    return 0;
};

int
create_always_allow (PGconn * resurs, char *sw_ip, char *profile_text,
                     char *rule_text, rdb * list, int type) {

    PGresult *result;
    char query[1024];
    int count;
    char bigstr[1024];
    write_log (3, "[%s] ACL_WORKS: create_always_allow() - started", sw_ip);
    /*
     * query = my_malloc (1024); bigstr = my_malloc (1024);
     */
    memset (query, 0, 1024);

    if (type == IMPB_UPDATE)
        sprintf (query,
                 "SELECT DISTINCT always_allow.ip, users.port from users, switches, always_allow where "
                 "switches.building=users.building "
                 "and switches.street=users.street and switches.accesspoint=users.accesspoint and "
                 "sw_ip='%s' order by users.port;", sw_ip);
    else
        sprintf (query,
                 "SELECT distinct always_allow.ip, sw_ports.sw_port from always_allow, sw_ports where "
                 "sw_ports.sw_ip='%s' order by sw_ports.sw_port;\n", sw_ip);

   	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

    for (count = 0; count < PQntuples (result); count++) {
        // config access_profile profile_id 40 add access_id %d ip
        // source_ip 0.0.0.0 destination_ip %s port %d permit
        memset (bigstr, '\0', 1024);
        sprintf (bigstr, "ip source_ip 0.0.0.0 destination_ip %s",
                 PQgetvalue (result, count, 0));
        rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 1)), 0, 40, PERMIT);
    }
    PQclear (result);
    /*
     * free (query); free (bigstr);
     */
    return 0;
};

//
//
// PROFILE 30 DST TCP PORT
//
//

int
create_ip_ports_tcp (PGconn * resurs, char *sw_ip, char *profile_text,
                     char *rule_text, rdb * list, int type) {

    PGresult *result;
    char query[1024];
    int count;
    char bigstr[1024];
    write_log (3, "[%s] ACL_WORKS: create_ip_ports_tcp() - started", sw_ip);
    /*
     * query = my_malloc (1024); bigstr = my_malloc (1024);
     */
    memset (query, 0, 1024);
    if (type == IMPB_UPDATE)
        sprintf (query,
                 "SELECT DISTINCT ip_ports.port, ip_ports.action, users.port from users, switches, ip_ports where "
                 "switches.building=users.building "
                 "and switches.street=users.street and switches.accesspoint=users.accesspoint and "
                 "sw_ip='%s' and ip_ports.type='tcp' order by users.port;", sw_ip);
    else
        sprintf (query, "SELECT distinct ip_ports.port, ip_ports.action,"
                 " sw_ports.sw_port from ip_ports, sw_ports where sw_ports.sw_ip='%s'"
                 " and type='tcp' order by sw_ports.sw_port;\n", sw_ip);

	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};


    for (count = 0; count < PQntuples (result); count++) {
        // config access_profile profile_id 30 add access_id %d ip tcp
        // dst_port %d port %d %s
        memset (bigstr, '\0', 1024);
        sprintf (bigstr, "ip tcp dst_port %d", atoi (PQgetvalue (result, count, 0)));
        if (strstr (PQgetvalue (result, count, 1), "permit"))
            rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 2)), 0, 30,
                        PERMIT);
        else
            rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 2)), 0, 30, DENY);

    }
    PQclear (result);
    /*
     * free (query); free (bigstr); *
     */
    return 0;
};

int
create_deny (PGconn * resurs, char *sw_ip, char *profile_text,
             char *rule_text, rdb * list) {

    PGresult *result;
    char query[1024];
    int count;
    char bigstr[1024];
    write_log (3, "[%s] ACL_WORKS: create_deny() - started", sw_ip);
    /*
     * query = my_malloc (1024); bigstr = my_malloc (1024);
     */
    memset (query, 0, 1024);
    sprintf (query, "SELECT distinct sw_port from sw_ports where sw_ip='%s';\n", sw_ip);

	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

    for (count = 0; count < PQntuples (result); count++) {
        // config access_profile profile_id 30 add access_id %d ip tcp
        // dst_port %d port %d %s
        memset (bigstr, '\0', 1024);
        sprintf (bigstr, "ip source_ip 0.0.0.0 destination_ip 0.0.0.0");
        rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 0)), 0, 60, DENY);

    }
    PQclear (result);
    /*
     * free (query); free (bigstr);
     */
    return 0;
};

int
create_ip_deny (PGconn * resurs, char *sw_ip, char *profile_text,
                char *rule_text, rdb * list) {

    PGresult *result;
    char query[1024];
    int count;
    char bigstr[1024];
    write_log (3, "[%s] ACL_WORKS: create_ip_deny() - started", sw_ip);
    /*
     * query = my_malloc (1024); bigstr = my_malloc (1024);
     */
    memset (query, 0, 1024);
    sprintf (query,
             "SELECT distinct port from users, switches  where switches.building=users.building "
             "and switches.street=users.street and switches.accesspoint=users.accesspoint "
             "and sw_ip='%s' order by port;\n", sw_ip);
	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

    for (count = 0; count < PQntuples (result); count++) {
        // config access_profile profile_id 30 add access_id %d ip tcp
        // dst_port %d port %d %s
        memset (bigstr, '\0', 1024);
        sprintf (bigstr, "ip source_ip 0.0.0.0 destination_ip 0.0.0.0");
        rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 0)), 0, 60, DENY);

    }
    PQclear (result);
    /*
     * free (query); free (bigstr);
     */
    return 0;
};

int
create_ip_ports_udp (PGconn * resurs, char *sw_ip, char *profile_text,
                     char *rule_text, rdb * list, int type) {

    PGresult *result;
    char query[1024];
    int count;
    char bigstr[1024];
    write_log (3, "[%s] ACL_WORKS: create_ip_ports_udp() - started", sw_ip);
    /*
     * query = my_malloc (1024); bigstr = my_malloc (1024);
     */
    memset (query, 0, 1024);
    if (type == IMPB_UPDATE)
        sprintf (query,
                 "SELECT DISTINCT ip_ports.port, ip_ports.action, users.port from users, switches, ip_ports where "
                 "switches.building=users.building "
                 "and switches.street=users.street and switches.accesspoint=users.accesspoint and "
                 "sw_ip='%s' and ip_ports.type='udp' order by users.port;", sw_ip);
    else
        sprintf (query, "SELECT distinct ip_ports.port, ip_ports.action,"
                 " sw_ports.sw_port from ip_ports, sw_ports where sw_ports.sw_ip='%s'"
                 " and type='udp' order by sw_ports.sw_port;\n", sw_ip);

	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

    for (count = 0; count < PQntuples (result); count++) {
        // config access_profile profile_id 20 add access_id %d ip udp
        // dst_port %d port %d %s
        memset (bigstr, '\0', 1024);
        sprintf (bigstr, "ip udp dst_port %d", atoi (PQgetvalue (result, count, 0)));
        if (strstr (PQgetvalue (result, count, 1), "permit"))
            rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 2)), 0, 20,
                        PERMIT);
        else
            rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 2)), 0, 20, DENY);
    }

    PQclear (result);
    /*
     * free (query); free (bigstr);
     */
    return 0;
};

int
create_users_permissions (PGconn * resurs, char *sw_ip, char *profile_text,
                          char *rule_text, rdb * list, int type) {

    PGresult *result;
    char query[1024];
    int count;
    char bigstr[1024];
    write_log (3, "[%s] ACL_WORKS: create_users_permissions() - started", sw_ip);
    /*
     * query = my_malloc (1024); bigstr = my_malloc (1024);
     */
    memset (query, 0, 1024);
    sprintf (query,
             "SELECT distinct users.ip, users_permissions.sw_port,users_permissions.action "
             "from users_permissions,users where users_permissions.login=users.login and "
             "users_permissions.sw_ip='%s';\n", sw_ip);

 	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

    for (count = 0; count < PQntuples (result); count++) {

        // config access_profile profile_id 10 add access_id %d ip
        // source_ip %s port %d %s
        memset (bigstr, '\0', 1024);
        sprintf (bigstr, "ip source_ip %s", PQgetvalue (result, count, 0));
        if (strstr (PQgetvalue (result, count, 2), "permit"))
            rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 1)), 0, 10,
                        PERMIT);
        else
            rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 1)), 0, 10, DENY);
    }

    PQclear (result);

    memset (query, '\0', 1024);
    sprintf (query,
             "SELECT distinct ip, ip_permissions.sw_port,ip_permissions.action from ip_permissions where sw_ip='%s';",
             sw_ip);

	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result = PQexec (resurs, query);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

    for (count = 0; count < PQntuples (result); count++) {

        // config access_profile profile_id 10 add access_id %d ip
        // source_ip %s port %d %s
        memset (bigstr, '\0', 1024);
        sprintf (bigstr, "ip source_ip %s", PQgetvalue (result, count, 0));
        if (strstr (PQgetvalue (result, count, 2), "permit"))
            rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 1)), 0, 10,
                        PERMIT);
        else
            rdb_insert (list, bigstr, atoi (PQgetvalue (result, count, 1)), 0, 10, DENY);
    }

    PQclear (result);
    /*
     * free (query); free (bigstr);
     */
    return 0;
};

rdb
make_rules_db (char *sw_ip, pdb * prof, PGconn * resurs, int act_type, int sw_type) {

    PGresult *result2;
    char query2[512];
    int cntr = 0;
    int count_a = 0;
    int profile_id = 0;
    char profile_text[256];
    char rule_text[256];
    rdb list = NULL;
    rdb temp = NULL;
    pdb profiles = NULL;
    int rules_count[8] = { 0 };
    int ix;
    int deny_state = 0;
    // query2 = my_malloc (512);
    memset (query2, '\0', 512);
    sprintf (query2, "SELECT DISTINCT deny_state FROM switches WHERE sw_ip='%s';", sw_ip);

	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result2 = PQexec (resurs, query2);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

    count_a = PQntuples (result2);
    if (count_a > 0)
        deny_state = atoi (PQgetvalue (result2, 0, 0));
    PQclear (result2);
    count_a = 0;

    memset (query2, '\0', 511);
    if (act_type == ACL_UPDATE && sw_type == DES3028) {
        sprintf (query2,
                 "SELECT DISTINCT ON (profile_id) profile_id, profile_text, rule_text FROM acl_3028;");
        act_type = IMPB_UPDATE;	// for 3028
    } else if (act_type == IMPB_UPDATE)
        sprintf (query2,
                 "SELECT DISTINCT ON (profile_id) profile_id, profile_text, rule_text FROM impb_profiles;");
    else
        sprintf (query2,
                 "SELECT DISTINCT ON (profile_id) profile_id, profile_text, rule_text FROM acl_profiles;");
	
	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result2 = PQexec (resurs, query2);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

    count_a = PQntuples (result2);
    for (cntr = 0; cntr < count_a; cntr++) {
        memset (profile_text, '\0', 256);
        memset (rule_text, '\0', 256);
        profile_id = atoi (PQgetvalue (result2, cntr, 0));
        strcpy (profile_text, PQgetvalue (result2, cntr, 1));
        strcat (profile_text, "\n");
        strcpy (rule_text, PQgetvalue (result2, cntr, 2));
        strcat (rule_text, "\n");
        // write(out, profile_text, strlen(profile_text));

        switch (profile_id) {
            case 10:
                pdb_insert (&profiles, profile_text);
                create_users_permissions (resurs, sw_ip, profile_text, rule_text, &list,
                                          act_type);
                break;

            case 20:
                pdb_insert (&profiles, profile_text);
                create_ip_ports_udp (resurs, sw_ip, profile_text, rule_text, &list, act_type);
                break;

            case 30:
                pdb_insert (&profiles, profile_text);
                create_ip_ports_tcp (resurs, sw_ip, profile_text, rule_text, &list, act_type);
                break;

            case 40:
                pdb_insert (&profiles, profile_text);
                create_always_allow (resurs, sw_ip, profile_text, rule_text, &list, act_type);
                break;

            case 50:
                pdb_insert (&profiles, profile_text);
                if (sw_type == DES3028)
                    create_ip_access_users (resurs, sw_ip, profile_text, rule_text, &list);
                else
                    create_access_users (resurs, sw_ip, profile_text, rule_text, &list);
                break;

            case 60:
                pdb_insert (&profiles, profile_text);
                if (deny_state > 0) {
                    if (act_type == IMPB_UPDATE && sw_type == DES3028) {
                        create_ip_deny (resurs, sw_ip, profile_text, rule_text, &list);
                    } else if (act_type == IMPB_UPDATE && sw_type != DES3028) {
                        create_eth_deny (resurs, sw_ip, profile_text, rule_text, &list);
                    } else {
                        create_deny (resurs, sw_ip, profile_text, rule_text, &list);
                    };
                };
                break;

                /*
                 * IMPB_UPDATE
                 */
            case 45:
                pdb_insert (&profiles, profile_text);
                create_arp_rule (resurs, sw_ip, profile_text, rule_text, &list);
                break;

            case 55:
                pdb_insert (&profiles, profile_text);
                create_ip_rule (resurs, sw_ip, profile_text, rule_text, &list);
                break;

        };
    };
    /*
     * free (rule_text); free (profile_text);
     */
    PQclear (result2);

    (*prof) = profiles;
    // ((c - 1) / 8)+1

    if (list) {
        temp = list->head;

        while (temp) {
            rules_count[(int) ((temp->port - 1) / 8)]++;	// incrementing rules
            // counter in section
            temp = temp->next;
        }

        for (ix = 0; ix < 8; ix++)
            if (rules_count[ix] >= 195)
                write_log (1, "(!) ACL_WORKS: [%s] Warning! Dangerous number of "
                           "rules (%d) in %d section", sw_ip, rules_count[ix], ix + 1);
        memset (query2, '\0', 511);
        sprintf (query2, "UPDATE rules_count SET p1=%d, p2=%d, p3=%d, p4=%d"
                 ", p5=%d, p6=%d, p7=%d, p8=%d WHERE sw_ip='%s';", rules_count[0],
                 rules_count[1], rules_count[2], rules_count[3], rules_count[4],
                 rules_count[5], rules_count[6], rules_count[7], sw_ip);

		if (0 != pthread_mutex_lock (&pg_mutex)) {
			DEBUG ("QUEUE Mutex lock failed!");
			exit (EXIT_FAILURE);
		};

        
        result2 = PQexec (resurs, query2);

		if (0 != pthread_mutex_unlock (&pg_mutex)) {
			DEBUG ("QUEUE Mutex unlock failed!");
			exit (EXIT_FAILURE);
		};

        
        /*
         * write_log(1, "[%s] BASE SAID: %s", sw_ip, PQerrorMessage(resurs));
         * write_log(1, "[%s] Request was: %s", sw_ip, query2);
         */
        PQclear (result2);

    }
    // free (query2);
    return list;
};

pdb
pdb_compare (pdb * sw_prof, pdb * db_prof) {
    pdb out = NULL,
              db_temp_prof = NULL,
                             sw_temp_prof = NULL,
                                            db_tail = NULL,
                                                      sw_tail = NULL;
    pdb db_save = (*db_prof);
    pdb sw_save = (*sw_prof);
    char *deleter;
    int cnt1 = 0,
               cnt2 = 0;

    if (sw_save && db_save) {
        db_temp_prof = db_save->head;
        sw_temp_prof = sw_save->head;
        while (db_temp_prof) {
            if ((sw_temp_prof = sw_save->head) != NULL)
                while (sw_temp_prof && db_temp_prof) {
                    if ((strcmp (sw_temp_prof->profile, db_temp_prof->profile) == 0)) {

                        /*
                         * if we have deleted the tail element in list, we must change
                         * the main list pointer
                         */
                        db_tail = db_temp_prof->prev;
                        sw_tail = sw_temp_prof->prev;
                        if ((cnt1 = pdb_delete (&db_temp_prof)) == 2)
                            db_save = db_tail;
                        if ((cnt2 = pdb_delete (&sw_temp_prof)) == 2)
                            sw_save = sw_tail;

                    } else if (sw_temp_prof)
                        sw_temp_prof = sw_temp_prof->next;
                    else
                        break;
                };
            if (db_temp_prof)
                db_temp_prof = db_temp_prof->next;
            else
                break;
            if (sw_save && cnt2 >= 0)
                sw_temp_prof = sw_save->head;
            else
                break;
        };
    };
    if (sw_save && cnt2 >= 0) {
        sw_temp_prof = sw_save->head;
        while (sw_temp_prof) {
            deleter = prof_delete (sw_temp_prof->profile);
            pdb_insert (&out, deleter);
            free (deleter);
            sw_temp_prof = sw_temp_prof->next;
        };
        pdb_destroy (&sw_save);
    } else
        (*sw_prof) = NULL;

    if (db_save && cnt1 >= 0) {
        db_temp_prof = db_save->head;
        while (db_temp_prof) {
            pdb_insert (&out, db_temp_prof->profile);
            db_temp_prof = db_temp_prof->next;
        };
        pdb_destroy (&db_save);
    } else
        (*db_prof) = NULL;
    return out;
};

rdb
rdb_compare (rdb * sw_config, rdb * db_config, pdb * Z, int fd) {
    rdb output = NULL;
    rdb sw_temp = NULL,
                  db_temp = NULL,
                            db_save = NULL,
                                      db_tail = NULL,
                                                sw_save = NULL,
                                                          sw_tail = NULL;

    pdb out = (*Z);
    int holes[7][1201];
    int x = 0,
            cnt1 = 0,
                   cnt2 = 0,
                          del_factor = 0;
    char *ready;
    int cmp_res = 0;

    /*
     *  HOLES ARRAY FOR MAKING RIGHT access_id IN NEW RULES
     */
    for (x = 0; x < 1200; x++) {
        holes[1][x] = 1;
        holes[2][x] = 1;
        holes[3][x] = 1;
        holes[4][x] = 1;
        holes[5][x] = 1;
        holes[6][x] = 1;
    }
    /*
     *  FINDING SAME RULES IN BASE AND DEVICE
     */
    db_save = (*db_config);
    sw_save = (*sw_config);

    if (db_save && sw_save) {
        db_temp = db_save->head;
        sw_temp = sw_save->head;
        while (db_temp && sw_temp) {
            del_factor = 0;	/* Variable will be set to 1 if we have deleted item from
				 * db_temp list (i.e. we have step to next item) */
            while (sw_temp && db_temp) {

                // cmp_res=compare_strings (sw_temp->string, db_temp->string);
                cmp_res = strcmp (sw_temp->string, db_temp->string);
                /*
                 * if (cmp_res >= 35) dprintf(fd, "strange: '%s'(port %d) not equal
                 * '%s'(port %d) at %d\n", db_temp->string, db_temp->port,
                 * sw_temp->string, sw_temp->port, cmp_res);
                 */
                if ((cmp_res == 0) && (db_temp->port == sw_temp->port)) {
                    int profid = (int) ((sw_temp->profile_id) / 10);
                    holes[profid][sw_temp->access_id] = 0;
                    /*
                     * Save the tails :)
                     */
                    dprintf (fd, "matched: '%s'(port %d) and '%s'(port %d) prof %d(%d)\n",
                             db_temp->string, db_temp->port, sw_temp->string,
                             sw_temp->port, db_temp->profile_id, sw_temp->profile_id);
                    db_tail = db_temp->prev;
                    sw_tail = sw_temp->prev;
                    cnt1 = rdb_delete (&db_temp);
                    cnt2 = rdb_delete (&sw_temp);
                    if (cnt1 == 2)
                        db_save = db_tail;
                    if (cnt2 == 2)
                        sw_save = sw_tail;
                    if (cnt1 == -1)
                        db_save = NULL;
                    if (cnt2 == -1)
                        sw_save = NULL;
                    del_factor = 1;
                } else {
                    if (sw_temp)
                        sw_temp = sw_temp->next;
                }
            };

            /*
             * If we have db_temp alive and have not delete items in this turn we should
             * step to next item in list (if we have deleted item, list have been turned
             * automaticly)
             */

            if (db_temp && del_factor == 0)
                db_temp = db_temp->next;

            if (sw_save)
                sw_temp = sw_save->head;

            else
                break;
            /*
             * if (sw_save && cnt2 >= 0) sw_temp = sw_save->head;
             */

        };
    };
    /*
     *  MAKE COMMANDS FOR DELETE UNNEEDED RULES FROM DEVICE
     */

    if (sw_save && cnt2 >= 0) {
        sw_temp = sw_save->head;
        while (sw_temp) {
            char *str = rule_delete (sw_temp->profile_id, sw_temp->access_id);
            pdb_insert (&out, str);
            free (str);
            sw_temp = sw_temp->next;
        };
        rdb_destroy (&sw_save);	/* destroying data in sw_save if we have it */
    } else
        (*sw_config) = NULL;
    /*
     *  MAKE COMMANDS FOR INSERT NEEDED RULES
     */
    if (db_save && cnt1 >= 0) {
        db_temp = db_save->head;
        int a = 1;
        int profid = 0;
        while (db_temp) {
            profid = (int) db_temp->profile_id / 10;
            for (a = 1; a < 1200; a++)
                if (holes[profid][a] == 1) {
                    db_temp->access_id = a;
                    holes[profid][a] = 0;
                    ready = rdb2rule (&db_temp);
                    pdb_insert (&out, ready);
                    free (ready);
                    break;
                }
            db_temp = db_temp->next;
        };
        rdb_destroy (&db_save);	/* destroy db_save */
    } else
        (*db_config) = NULL;

    (*Z) = out;
    return output;
};

char *
prof_delete (char *string) {
    char *result;
    char *ptr;
    char *preres;
    char *prof_del = "delete access_profile ";
    ptr = string;
    result = my_malloc (strlen (string) + 50);
    preres = result;
    memset (result, '\0', strlen (string) + 50);
    strcpy (preres, prof_del);
    ptr = strstr (string, "profile_id");
    strcat (preres, ptr);
    return preres;
};

char *
rule_delete (int profid, int accid) {
    char *delete = "config access_profile profile_id %d delete access_id %d\n";
    char *result = my_malloc (100);
    sprintf (result, delete, profid, accid);
    return result;
};

char *
rdb2rule (rdb * list) {
    char act[][8] = { "deny\n",
                      "permit\n"
                    };
    char *tmp;
    tmp = my_malloc (300);
    memset (tmp, '\0', 300);
    char prt[20];
    memset (prt, '\0', 20);
    sprintf (tmp, "config access_profile profile_id %d add access_id %d ",
             (*list)->profile_id, (*list)->access_id);
    strcat (tmp, (*list)->string);
    sprintf (prt, " port %d ", (*list)->port);
    strcat (tmp, prt);
    strcat (tmp, act[(*list)->action]);
    return tmp;
};

rdb
make_impb_db (char *sw_ip, pdb * prof) {
    extern opt *opts;
    PGconn *resurs;
    PGresult *result2;
    char *query2;
    int cntr = 0;
    int count_a = 0;
    int profile_id = 0;
    char *profile_text;
    char *rule_text;
    char conn_data[250];
    rdb list = NULL;
    rdb temp = NULL;
    pdb profiles = NULL;
    int rules_count[8] = { 0 };
    int ix;

    sprintf (conn_data, "dbname=%s", (*opts).dbname);
    resurs = PQconnectdb (conn_data);

    if (PQstatus (resurs) == CONNECTION_OK)
        write_log (1,
                   "ACL_WORKS: [%s] make_rules_db() : connection to database established",
                   sw_ip);
    else {
        write_log (1,
                   "ACL_WORKS: (!) make_rules_db: connection to database failed: %s",
                   PQerrorMessage (resurs));
        PQfinish (resurs);
        return NULL;
    };

    query2 = my_malloc (512);
    memset (query2, 0, 511);
    sprintf (query2,
             "SELECT DISTINCT ON (profile_id) profile_id, profile_text, rule_text FROM acl_profiles;");

	if (0 != pthread_mutex_lock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex lock failed!");
		exit (EXIT_FAILURE);
	};

    result2 = PQexec (resurs, query2);

	if (0 != pthread_mutex_unlock (&pg_mutex)) {
		DEBUG ("QUEUE Mutex unlock failed!");
		exit (EXIT_FAILURE);
	};

    rule_text = my_malloc (256);
    profile_text = my_malloc (256);
    count_a = PQntuples (result2);
    for (cntr = 0; cntr < count_a; cntr++) {
        memset (profile_text, '\0', 255);
        memset (rule_text, '\0', 255);
        profile_id = atoi (PQgetvalue (result2, cntr, 0));
        strcpy (profile_text, PQgetvalue (result2, cntr, 1));
        strcat (profile_text, "\n");
        strcpy (rule_text, PQgetvalue (result2, cntr, 2));
        strcat (rule_text, "\n");
        // write(out, profile_text, strlen(profile_text));

        switch (profile_id) {
            case 10:
                pdb_insert (&profiles, profile_text);
                create_users_permissions (resurs, sw_ip, profile_text, rule_text, &list,
                                          ACL_UPDATE);
                break;

            case 20:
                pdb_insert (&profiles, profile_text);
                create_ip_ports_udp (resurs, sw_ip, profile_text, rule_text, &list,
                                     ACL_UPDATE);
                break;

            case 30:
                pdb_insert (&profiles, profile_text);
                create_ip_ports_tcp (resurs, sw_ip, profile_text, rule_text, &list,
                                     ACL_UPDATE);
                break;

            case 40:
                pdb_insert (&profiles, profile_text);
                create_always_allow (resurs, sw_ip, profile_text, rule_text, &list,
                                     ACL_UPDATE);
                break;

            case 50:
                pdb_insert (&profiles, profile_text);
                create_access_users (resurs, sw_ip, profile_text, rule_text, &list);
                break;

            case 60:
                pdb_insert (&profiles, profile_text);
                create_deny (resurs, sw_ip, profile_text, rule_text, &list);
                break;
        };
    };

    free (rule_text);
    free (profile_text);
    free (query2);
    PQfinish (resurs);
    PQclear (result2);
    (*prof) = profiles;
    // ((c - 1) / 8)+1

    temp = list->head;

    while (temp) {
        rules_count[(int) ((temp->port - 1) / 8)]++;	// incrementing rules counter in
        // section
        temp = temp->next;
    }

    for (ix = 0; ix < 8; ix++)
        if (rules_count[ix] >= 195)
            write_log (1, "(!) ACL_WORKS: [%s] Warning! Dangerous number of "
                       "rules (%d) in %d section", sw_ip, rules_count[ix], ix + 1);

    return list;
};

pdb
impb_compare (pdb * sw_prof, pdb * db_prof, pdb * sw_rules, pdb * db_rules, int fd) {
    pdb out = NULL,
              db_temp_prof = NULL,
                             sw_temp_prof = NULL,
                                            db_tail = NULL,
                                                      sw_tail = NULL;
    pdb db_temp_rules = NULL,
                        sw_temp_rules = NULL,
                                        db_rules_tail = NULL,
                                                        sw_rules_tail = NULL;

    pdb db_save = (*db_prof);
    pdb sw_save = (*sw_prof);
    pdb dbrules_save = (*db_rules);
    pdb swrules_save = (*sw_rules);

    char *deleter;
    int cnt1 = 0,
               cnt2 = 0,
                      del_factor = 0,
                                   retcode = 0;

    if (sw_save && db_save) {
        db_temp_prof = db_save->head;
        sw_temp_prof = sw_save->head;
        while (db_temp_prof && sw_temp_prof) {
            del_factor = 0;
            // if ((sw_temp_prof = sw_save->head) != NULL)
            while (sw_temp_prof && db_temp_prof) {
                if ((compare_strings (sw_temp_prof->profile, db_temp_prof->profile) == 0)) {

                    /*
                     * if we have deleted the tail element in list, we must change the
                     * main list pointer
                     */
                    db_tail = db_temp_prof->prev;
                    sw_tail = sw_temp_prof->prev;

                    cnt1 = pdb_delete (&db_temp_prof);
                    cnt2 = pdb_delete (&sw_temp_prof);
                    if (cnt1 == 2)
                        db_save = db_tail;
                    if (cnt2 == 2)
                        sw_save = sw_tail;
                    if (cnt1 == -1)
                        db_save = NULL;
                    if (cnt2 == -1)
                        sw_save = NULL;
                    del_factor = 1;

                } else if (sw_temp_prof)
                    sw_temp_prof = sw_temp_prof->next;
                // else break;
            };
            if (db_temp_prof && del_factor == 0)
                db_temp_prof = db_temp_prof->next;
            if (sw_save)
                sw_temp_prof = sw_save->head;
            else
                break;
        };
    };
    if (sw_save && cnt2 >= 0) {
        sw_temp_prof = sw_save->head;
        while (sw_temp_prof) {
            deleter = impb_prof_delete (sw_temp_prof->profile);
            pdb_insert (&out, deleter);
            free (deleter);
            sw_temp_prof = sw_temp_prof->next;
        };
        pdb_destroy (&sw_save);
    } else
        (*sw_prof) = NULL;

    if (db_save && cnt1 >= 0) {
        db_temp_prof = db_save->head;
        while (db_temp_prof) {
            pdb_insert (&out, db_temp_prof->profile);
            db_temp_prof = db_temp_prof->next;
        };
        pdb_destroy (&db_save);
    } else
        (*db_prof) = NULL;
    /*
     *
     *
     *  RULES CMP
     *
     */
    cnt1 = 0, cnt2 = 0;
    if (swrules_save && dbrules_save) {
        db_temp_rules = dbrules_save->head;
        sw_temp_rules = swrules_save->head;
        while (db_temp_rules) {
            if ((sw_temp_rules = swrules_save->head) != NULL)
                while (sw_temp_rules && db_temp_rules) {
                    del_factor = 0;
                    retcode = strcasecmp (sw_temp_rules->profile, db_temp_rules->profile);
                    if (retcode == 0) {
                        /*
                         * if we have deleted the tail element in list, we must change
                         * the main list pointer
                         */
                        db_rules_tail = db_temp_rules->prev;
                        sw_rules_tail = sw_temp_rules->prev;
                        cnt1 = pdb_delete (&db_temp_rules);
                        cnt2 = pdb_delete (&sw_temp_rules);
                        if (cnt1 == 2)
                            dbrules_save = db_rules_tail;
                        if (cnt2 == 2)
                            swrules_save = sw_rules_tail;
                        if (cnt1 == -1)
                            dbrules_save = NULL;
                        if (cnt2 == -1)
                            swrules_save = NULL;
                        del_factor = 1;
                    } else if (sw_temp_rules)
                        sw_temp_rules = sw_temp_rules->next;
                };
            if (db_temp_rules && del_factor == 0)
                db_temp_rules = db_temp_rules->next;
            if (swrules_save)
                sw_temp_rules = swrules_save->head;
            else
                break;
        };
    };

    if (swrules_save && cnt2 >= 0) {
        sw_temp_rules = swrules_save->head;
        while (sw_temp_rules) {
            deleter = impb_rule_delete (sw_temp_rules->profile);
            pdb_insert (&out, deleter);
            free (deleter);
            sw_temp_rules = sw_temp_rules->next;
        };
        pdb_destroy (&swrules_save);
    } else
        (*sw_rules) = NULL;

    if (dbrules_save && cnt1 >= 0) {
        db_temp_rules = dbrules_save->head;
        while (db_temp_rules) {
            pdb_insert (&out, db_temp_rules->profile);
            db_temp_rules = db_temp_rules->next;
        };
        pdb_destroy (&dbrules_save);
    } else
        (*db_rules) = NULL;

    return out;
};

char *
impb_prof_delete (char *string) {
    /*
     *  config address_binding ip_mac ports 10 state disable allow_zeroip disable
     *  config address_binding ip_mac ports 10 state enable allow_zeroip enable
     */

    char *ptr,
    *runner,
    *out,
    *out_s;
    char *del = "state disable allow_zeroip disable\n";
    ptr = string;
    int len = strlen (string);
    out = my_malloc (len + 5);
    // out_s = out;
    out_s = memset (out, '\0', len + 5);
    ptr = strstr (string, "state");
    for (runner = string; runner != ptr; *runner++) {
        (*out++) = (*runner);
    }
    strcat (out_s, del);
    return out_s;
};

char *
impb_rule_delete (char *string) {
    /*
     * create address_binding ip_mac ipaddr 172.11.0.11 mac_address AA-BB-CC-11-AA-EE
     * ports 2
     */
    char *tmp,
    *out,
    *mark,
    *runner,
    *out_s,
    *last;
    char *del = "delete ";
    int len = strlen (string);
    out = my_malloc (len + 10);
    out_s = memset (out, '\0', len + 10);
    tmp = strstr (string, "address_binding");
    mark = strstr (string, "ports");

    for (runner = del; *runner != '\0'; *runner++)
        (*out++) = (*runner);
    for (runner = tmp; runner != mark; *runner++)
        (*out++) = (*runner);

    strcat (out_s, "\n");
    return out_s;
};
