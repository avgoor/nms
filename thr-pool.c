/*
 *      thr-pool.c
 *
 *      Copyright 2008 Avgoor <avgoor@noc.cable-city.net>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include "nms.h"

#define MAXT_IN_POOL 300
#define	MAX_QUEUE_MEMORY_SIZE	131072

struct qhead *
            generate_queue (int initial_cap) {

    qhead *grand_queue = (qhead *) malloc (sizeof (qhead));
    int max_cap = MAX_QUEUE_MEMORY_SIZE / (sizeof (qnode)),
                  i;
    qnode *temp;

    if (grand_queue == NULL) {
        DEBUG ("Can't allocate memory!");
        exit (2);
    }

    if (initial_cap > max_cap) {
        initial_cap = max_cap;
    }

    if (initial_cap == 0) {
        DEBUG ("Zero-sized queue");
        exit (2);
    }

    grand_queue->capacity = initial_cap;

    grand_queue->max_capacity = max_cap;
    grand_queue->head = NULL;
    grand_queue->tail = NULL;
    grand_queue->fhead = (qnode *) malloc (sizeof (qnode));

    if (grand_queue->fhead == NULL) {
        DEBUG ("Can't allocate memory");
        exit (2);
    }

    grand_queue->ftail = grand_queue->fhead;

    /*
     * fill "free queue"
     */

    for (i = 1; i <= initial_cap; i++) {
        temp = (qnode *) malloc (sizeof (qnode));

        if (temp == NULL) {
            DEBUG ("Can't allocate memory");
            exit (2);
        }

        temp->func_to_dispatch = NULL;
        temp->func_arg = NULL;
        temp->cleanup_func = NULL;
        temp->cleanup_arg = NULL;

        temp->next = grand_queue->fhead;

        temp->prev = NULL;
        grand_queue->fhead->prev = temp;
        grand_queue->fhead = temp;
    }

    return grand_queue;
}

void
insert_job (qhead * grand_queue, dispatch_fn func1, void *arg1, dispatch_fn func2,
            void *arg2) {
    qnode *temp;

    if (grand_queue->ftail == NULL) {
        temp = (qnode *) malloc (sizeof (qnode));
        write_log (1, "THR-POOL: malloc() for new node");
        if (temp == NULL) {
            DEBUG ("Can't allocate memory");
            exit (2);
        }

        temp->next = NULL;

        temp->prev = NULL;
        grand_queue->fhead = temp;
        grand_queue->ftail = temp;
        grand_queue->capacity++;
    }

    temp = grand_queue->ftail;

    if (grand_queue->ftail->prev == NULL) {
        grand_queue->ftail = NULL;
        grand_queue->fhead = NULL;

    } else {
        grand_queue->ftail->prev->next = NULL;
        grand_queue->ftail = grand_queue->ftail->prev;
        grand_queue->ftail->next = NULL;
    }

    temp->func_to_dispatch = func1;

    temp->func_arg = arg1;
    temp->cleanup_func = func2;
    temp->cleanup_arg = arg2;

    temp->prev = NULL;

    if (grand_queue->head == NULL) {
        grand_queue->tail = temp;
        grand_queue->head = temp;

    } else {
        temp->next = grand_queue->head;
        grand_queue->head->prev = temp;
        grand_queue->head = temp;
    }
}

void
take_job (qhead * grand_queue, dispatch_fn * func1, void **arg1, dispatch_fn * func2,
          void **arg2) {
    qnode *temp = NULL;

    temp = grand_queue->tail;

    if (temp == NULL) {
        DEBUG ("Can't take job from empty queue");
        exit (2);
    }

    if (grand_queue->tail->prev == NULL) {
        grand_queue->tail = NULL;
        grand_queue->head = NULL;

    } else {
        grand_queue->tail->prev->next = NULL;
        grand_queue->tail = grand_queue->tail->prev;
        grand_queue->tail->next = NULL;
    }

    *func1 = temp->func_to_dispatch;
    *arg1 = temp->func_arg;
    *func2 = temp->cleanup_func;
    *arg2 = temp->cleanup_arg;

    temp->func_to_dispatch = NULL;
    temp->func_arg = NULL;
    temp->cleanup_func = NULL;
    temp->cleanup_arg = NULL;

    temp->next = NULL;

    if (grand_queue->fhead == NULL) {
        grand_queue->ftail = temp;
        grand_queue->fhead = temp;
        temp->prev = NULL;

    } else {
        temp->next = grand_queue->fhead;
        grand_queue->fhead->prev = temp;
        grand_queue->fhead = temp;
    }
}

int
queue_not_full (struct qhead *grand_queue) {
    return (grand_queue->ftail != NULL
            || grand_queue->capacity <= grand_queue->max_capacity);
}

int
there_is_job (struct qhead *grand_queue) {
    return (grand_queue->tail != NULL);
}

void *
do_work (void *owning_pool) {
    // Convert pointer to owning pool to proper type.
    _threadpool *pool = (_threadpool *) owning_pool;
    extern _dev_lock main_lock;
    main_lock = NULL;
    // Remember my creation sequence number
    int myid = pool->live;
	int status=0;


    // When we get a posted job, we copy it into these local vars.
    dispatch_fn myjob = NULL;
    void *myarg = NULL;
    dispatch_fn mycleaner = NULL;
    void *mycleanarg = NULL;
    char conn_info[255];
    char *storage;

    PGconn *serv_conn;

    fdb_data *mydata = NULL;

    extern opt *opts;

    pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

    pthread_cleanup_push ((void *) pthread_mutex_unlock, (void *) &pool->mutex);

    sprintf (conn_info, "dbname=%s", opts->dbname);

/*
    serv_conn = PQconnectdb (conn_info);

    if (PQstatus (serv_conn) == CONNECTION_OK) {
    } else {
        write_log (1, "PG_DRIVER: (!) connection to database failed: %s",
                   PQerrorMessage (res));
        PQfinish (serv_conn);
        return (NULL);
    };

*/

    storage = my_malloc (opts->malloc_size + 1);
    if (NULL == storage) {
        DEBUG ("Can't allocate storage");
        return (NULL);
    };
    // Grab mutex so we can begin waiting for a job

    if (0 != pthread_mutex_lock (&pool->mutex)) {
        DEBUG ("Mutex lock failed!");
        exit (EXIT_FAILURE);
    }
    // Main loop: wait for job posting, do job(s) ... forever
    for (;;) {

        while (there_is_job (pool->job_queue) == 0) {
/*           if(pthread_cond_wait (&pool->job_posted, &pool->mutex) == EPERM) {
				DEBUG ("ptread_cond_wait() error: mutex not own by thread");
	           	goto end_of_work;
           	}
*/			status=pthread_cond_wait (&pool->job_posted, &pool->mutex);
			write_log(1, "%d on pthread_cond_wait(), %d on there_is_job()", status, there_is_job (pool->job_queue));
        }

//		write_log(1, "%d on trylock", pthread_mutex_trylock(&pool->mutex));

        // We've just woken up and we have the mutex.  Check pool's state
        if (ALL_EXIT == pool->state)
            break;

        myjob = NULL;
        myarg = NULL;


		/*Workaround to try if trouble was gained by the concurent calls
		 * between dispatcher and workers to job_queue*/
		 
		if (0 != pthread_mutex_lock (&pool->queue_mutex)) {
            DEBUG ("QUEUE Mutex lock failed!");
            exit (EXIT_FAILURE);
        };
		
        // while we find work to do
        if (pool->job_queue->tail != NULL)
            take_job (pool->job_queue, &myjob, &myarg, &mycleaner, &mycleanarg);
        else
            DEBUG ("Empty queue, but pthread_cond_wait released!");

		if (0 != pthread_mutex_unlock (&pool->queue_mutex)) {
            DEBUG ("QUEUE Mutex unlock failed!");
            exit (EXIT_FAILURE);
        };

        
        pthread_cond_signal (&pool->job_taken);

        // Yield mutex so other jobs can be posted
        if (0 != pthread_mutex_unlock (&pool->mutex)) {
            DEBUG ("Mutex unlock failed!");
            exit (EXIT_FAILURE);
        };

        if ((myjob == NULL) && (myarg == NULL)) {
            DEBUG ("Empty job & arg!");
        } else {
        	// Run the job we've taken
	        mydata = (fdb_data *) myarg;

/*
	        mydata->resurs = serv_conn;
*/

	        storage = memset (storage, 0, opts->malloc_size);
	        mydata->storage_ptr = storage;
	
	        write_log (1, "%s:%d, [%u] got job for %s", __FILE__, __LINE__, myid,
	                   mydata->name);
	        dev_lockup (mydata->name, &main_lock);
	        if (mycleaner != NULL) {
	            pthread_cleanup_push (mycleaner, mycleanarg);
	            myjob ((void *) mydata);
	            pthread_cleanup_pop (1);
	        } else {
	            myjob ((void *) mydata);
	        }
	        dev_unlock (mydata->name, &main_lock);
	        mydata->resurs = NULL;
	        mydata->storage_ptr = NULL;
	        free (mydata);
		};

end_of_work:
        // Grab mutex so we can grab posted job, or (if no job is posted)
        // begin waiting for next posting.
        if (0 != pthread_mutex_lock (&pool->mutex)) {
            DEBUG ("Mutex lock failed!");
            exit (EXIT_FAILURE);
        }
    }

    // If we get here, we broke from loop because state is ALL_EXIT.
    --pool->live;

    free (storage);
/*
    PQfinish (serv_conn);
*/
    // We're not really taking a job ... but this signals the destroyer
    // that one thread has exited, so it can keep on destroying.
    pthread_cond_signal (&pool->job_taken);

    if (0 != pthread_mutex_unlock (&pool->mutex)) {
        DEBUG ("Mutex unlock failed!");
        exit (EXIT_FAILURE);
    }
    pthread_cleanup_pop (1);
    return NULL;
}

threadpool
create_threadpool (int num_threads_in_pool) {
    _threadpool *pool;		// pool we create and hand back
    int i;			// work var
    pthread_attr_t thread_attr;

    // sanity check the argument
    if ((num_threads_in_pool <= 0) || (num_threads_in_pool > MAXT_IN_POOL))
        return NULL;

    // create the _threadpool struct
    pool = (_threadpool *) my_malloc (sizeof (_threadpool));

    if (pool == NULL) {
        DEBUG ("Malloc failed!");
        return NULL;
    }

    memset (pool, 0, sizeof (_threadpool));

    // initialize everything but the array and live thread count
    pthread_mutex_init (&(pool->mutex), NULL);
    pthread_mutex_init (&(pool->queue_mutex), NULL);
    pthread_cond_init (&(pool->job_posted), NULL);
    pthread_cond_init (&(pool->job_taken), NULL);
    pool->arrsz = num_threads_in_pool;
    pool->state = ALL_RUN;
    pool->job_queue = generate_queue (num_threads_in_pool);
    pthread_attr_init (&thread_attr);
    pthread_attr_setstacksize (&thread_attr, 1024 * 1024);
    gettimeofday (&pool->created, NULL);

    // create the array of threads within the pool
    pool->array = (pthread_t *) malloc (pool->arrsz * sizeof (pthread_t));
    if (NULL == pool->array) {
        DEBUG ("Malloc failed");
        free (pool);
        pool = NULL;
        return NULL;
    }
    // bring each thread to life (update counters in loop so threads can
    // access pool->live to find out their ID#
    for (i = 0; i < pool->arrsz; ++i) {
        if (0 != pthread_create (pool->array + i, &thread_attr, do_work, (void *) pool)) {
            DEBUG ("Cannot create thread");
            exit (EXIT_FAILURE);
        }
        pool->live = i + 1;
        pthread_detach (pool->array[i]);	// automatic cleanup when thread exits.
    }
    DEBUG ("Thread pool succefully started");
    return (threadpool) pool;
}

void
dispatch (threadpool from_me, dispatch_fn dispatch_to_here, void *arg) {
    _threadpool *pool = (_threadpool *) from_me;
    // int old_cancel;
    if (pool == (_threadpool *) arg) {
    } else {
        pthread_cleanup_push (pthread_mutex_unlock, (void *) &pool->mutex);

        // Grab the mutex
        if (0 != pthread_mutex_lock (&pool->mutex)) {
            DEBUG ("Lock failed");
            exit (-1);
        }

        while (!queue_not_full (pool->job_queue)) {
            pthread_cond_signal (&pool->job_posted);
            pthread_cond_wait (&pool->job_taken, &pool->mutex);
            DEBUG ("WAITING room for job");
        }


		/* Workaround to take out concurency in job_queue changes */
		
		if (0 != pthread_mutex_lock (&pool->queue_mutex)) {
            DEBUG ("QUEUE Mutex lock failed!");
            exit (EXIT_FAILURE);
        };

		
        // Finally, there's room to post a job. Do so and signal workers.
        if ((NULL != arg) && (NULL != dispatch_to_here)) {
            insert_job (pool->job_queue, dispatch_to_here, arg, NULL, NULL);
            pthread_cond_signal (&pool->job_posted);
        } else {
            DEBUG ("Trying to dispatch with NULL!");
        };

		if (0 != pthread_mutex_unlock (&pool->queue_mutex)) {
            DEBUG ("QUEUE Mutex unlock failed!");
            exit (EXIT_FAILURE);
        };


        // Yield mutex so a worker can pick up the job
        if (0 != pthread_mutex_unlock (&pool->mutex)) {
            DEBUG ("Unlock failed");
            exit (EXIT_FAILURE);
        }
        pthread_cleanup_pop (1);
    }
}

void
destroy_threadpool (threadpool destroyme) {
    _threadpool *pool = (_threadpool *) destroyme;
    int oldtype;

    pthread_setcanceltype (PTHREAD_CANCEL_DEFERRED, &oldtype);
    pthread_cleanup_push ((void *) pthread_mutex_unlock, (void *) &pool->mutex);

    // Cause all threads to exit. Because they were detached when created,
    // the underlying memory for each is automatically reclaimed.

    // Grab the mutex
    if (0 != pthread_mutex_lock (&pool->mutex)) {
        DEBUG ("Mutex lock failed!");
        exit (-1);
    }

    pool->state = ALL_EXIT;

    while (pool->live > 0) {
        // get workers to check in ...
        pthread_cond_signal (&pool->job_posted);
        // ... and wake up when they check out.
        pthread_cond_wait (&pool->job_taken, &pool->mutex);
    }

    // Null-out entries in pool's thread array; free array.
    memset (pool->array, 0, pool->arrsz * sizeof (pthread_t));
    free (pool->array);

    // Destroy the mutex and condition variables in the pool.

    pthread_cleanup_pop (0);
    if (0 != pthread_mutex_unlock (&pool->mutex)) {
        DEBUG ("Mutex unlock failed!");
        exit (EXIT_FAILURE);
    }

    if (0 != pthread_mutex_destroy (&pool->mutex)) {
        DEBUG ("Mutex destruction failed!");
        exit (EXIT_FAILURE);
    }

    if (0 != pthread_cond_destroy (&pool->job_posted)) {
        DEBUG ("Condition Variable 'job_posted' destruction failed!");
        exit (EXIT_FAILURE);
    }

    if (0 != pthread_cond_destroy (&pool->job_taken)) {
        DEBUG ("Condition Variable 'job_taken' destruction failed!");
        exit (EXIT_FAILURE);
    }
    // Zero out all bytes of the pool
    memset (pool, 0, sizeof (_threadpool));

    // Free the pool and null out the pointer to it
    free (pool);
    pool = NULL;
    destroyme = NULL;
}
