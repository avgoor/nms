
/***************************************************************************
 *   Copyright (C) 2008 by Denis V. Meltsaykin   			   *
 *   dvm.avgoor@gmail.com   						   *
 *                                                                         *
 *   Permission is hereby granted, free of charge, to any person obtaining *
 *   a copy of this software and associated documentation files (the       *
 *   "Software"), to deal in the Software without restriction, including   *
 *   without limitation the rights to use, copy, modify, merge, publish,   *
 *   distribute, sublicense, and/or sell copies of the Software, and to    *
 *   permit persons to whom the Software is furnished to do so, subject to *
 *   the following conditions:                                             *
 *                                                                         *
 *   The above copyright notice and this permission notice shall be        *
 *   included in all copies or substantial portions of the Software.       *
 *                                                                         *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       *
 *   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    *
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*
 *   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR     *
 *   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, *
 *   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR *
 *   OTHER DEALINGS IN THE SOFTWARE.                                       *
 ***************************************************************************/

#include "nms.h"

// Global definitions

PGconn *res;
PGresult *result;

extern int
    open_db (opt * options) {
    char conn_data[255];

    if ((!options->dbname) || (!options->dbpass) ||
            (!options->dbhost) || (!options->dblogin)) {
        write_log (1, "PG_DRIVER: (!) not enough options");
        return (10);
    }
    /*
     * sprintf(conn_data, "dbname=%s host=%s user=%s password=%s",
     * options->dbname, options->dbhost, options->dblogin,
     * options->dbpass);
     */
    sprintf (conn_data, "dbname=%s", (*options).dbname);

    res = PQconnectdb (conn_data);

    // free(conn_data);

    if (PQstatus (res) == CONNECTION_OK)
        write_log (1, "PG_DRIVER: connection to database established");
    else {
        write_log (1, "PG_DRIVER: (!) connection to database failed: %s",
                   PQerrorMessage (res));
        PQfinish (res);
        return (-1);
    };
    return (1);
};

extern int
    close_db () {
    if (PQstatus (res) == CONNECTION_OK) {
        PQfinish (res);
        // free(res);
        write_log (1, "PG_DRIVER: connection to database closed");
    } else {
        write_log (1, "PG_DRIVER: invalid close");
    };
    return 0;
};

extern int
    save_fdb (fdb list1, char *sw_ip, PGconn * resurs) {

    char pl_create[] =
        "DROP FUNCTION merge_fdb (integer, integer, text, text);\n"
        "CREATE FUNCTION merge_fdb(inport INT, invlan INT, inmac TEXT, insw_ip TEXT) RETURNS VOID AS\n"
        "$$\n"
        "BEGIN\n"
        "LOOP\n"
        "UPDATE fdb SET seen_time = (date_trunc( 'second' , 'now'::timestamp )) WHERE mac = inmac and sw_ip = insw_ip"
        " and sw_port = inport and vlan = invlan;\n"
        "IF found THEN\n"
        "RETURN;\n"
        "END IF;\n"
        "BEGIN\n"
        "INSERT INTO fdb(mac, sw_ip, sw_port, vlan, seen_time) VALUES (inmac, insw_ip, inport, invlan, date_trunc( 'second' , 'now'::timestamp ));\n"
        "RETURN;\n"
        "EXCEPTION WHEN unique_violation THEN\n"
        "END;\n" "END LOOP;\n" "END;\n" "$$\n" "LANGUAGE plpgsql;\n";

    char query2[512];
    fdb list;
    list = list1->head;
    int c = 0;
    PGresult *result2;
    while (list) {
        memset (query2, 0, 511);
        /*
         *              sprintf (query2,
         *                      "INSERT INTO fdb (sw_ip, sw_port, mac, vlan) SELECT '%s',"
         *                      "'%d', '%s' , '%d' WHERE NOT EXISTS (SELECT sw_ip,"
         *                      "sw_port, mac, vlan FROM fdb WHERE (mac = '%s') AND (sw_ip = '%s') AND (sw_port='%d'));"
         *                      " UPDATE fdb set sw_ip='%s', sw_port='%d', vlan='%d',"
         *                      " seen_time=(date_trunc( 'second' , 'now'::timestamp )) where mac='%s';\n",
         *                      sw_ip, list->port, list->mac, list->vlan, list->mac,
         *                      sw_ip, list->port, sw_ip, list->port, list->vlan, list->mac);
         */
        /*
         * merge_fdb(inport INT, invlan INT, inmac TEXT, insw_ip TEXT)
         */
        sprintf (query2, "SELECT merge_fdb (%d, %d, '%s', '%s');", list->port, list->vlan,
                 list->mac, sw_ip);

		if (0 != pthread_mutex_lock (&pg_mutex)) {
			DEBUG ("QUEUE Mutex lock failed!");
			exit (EXIT_FAILURE);
		};

        result2 = PQexec (resurs, query2);
        
		if (0 != pthread_mutex_unlock (&pg_mutex)) {
			DEBUG ("QUEUE Mutex unlock failed!");
			exit (EXIT_FAILURE);
		};

        
        // printf("%s ", PQresultErrorMessage(result2));
        list = list->next;
        c++;
        PQclear (result2);
    };

    write_log (3, "PG_DRIVER: [%s] save_fdb() made %d queries", sw_ip, c);

    return 0;
};
